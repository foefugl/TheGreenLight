﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TestCustomFieldScript))]
public class TestCustomFieldScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var script = target as TestCustomFieldScript;
        script.flag = GUILayout.Toggle(script.flag, "Flag");
        // if(script.flag)

    }
}
