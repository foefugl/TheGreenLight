﻿using UnityEngine;

[ExecuteInEditMode]
public class DrawArea : MonoBehaviour
{
    public Color AreaColor;
    public Transform overlookObject;
    public Transform[] points;
    private Vector3[] pointsPos;
    private void Update()
    {
        pointsPos = new Vector3[points.Length];
        for(int i=0 ; i<points.Length ; i++)
        {
            pointsPos[i] = points[i].position;
            if(points[i].transform.localPosition.y != 0)
            {
                Vector3 currentPointLocalPos = points[i].localPosition;
                points[i].transform.localPosition = new Vector3(currentPointLocalPos.x, 0, currentPointLocalPos.z);
            }
        }

        if(overlookObject != null)
            Debug.Log(ContainsPoint(pointsPos, overlookObject.position));
    }
    public bool ContainsPoint(Vector3[] polyPoints, Vector3 p)
    {
        var j = polyPoints.Length - 1;
        var inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            var pi = polyPoints[i];
            var pj = polyPoints[j];
            if (((pi.z <= p.z && p.z < pj.z) || (pj.z <= p.z && p.z < pi.z)) &&
                (p.x < (pj.x - pi.x) * (p.z - pi.z) / (pj.z - pi.z) + pi.x))
                inside = !inside;
        }
        return inside;
    }
}
