﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DrawArea))]
public class DrawAreaEditor : Editor
{
    void OnSceneGUI()
    {
        DrawArea t = target as DrawArea;

        if(t == null || 
        (t.points.Length <= 3 && (t.points[0] == null || t.points[1] == null || t.points[2] == null)))
            return;

        Vector3[] drawArea = new Vector3[t.points.Length];
        for(int i=0 ; i< drawArea.Length ; i++)
        {
            drawArea[i] = t.points[i].position;
        }

        Handles.color = t.AreaColor;
        Handles.DrawAAConvexPolygon(drawArea);
    }
}
