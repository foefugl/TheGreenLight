﻿using UnityEngine;

[ExecuteInEditMode]
public class DrawArrow : MonoBehaviour
{
    public GameObject From, To;
}
