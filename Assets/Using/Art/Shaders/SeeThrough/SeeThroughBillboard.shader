﻿Shader "Custom/SeeThroughBillboard"
{
    Properties
	{
        _HighLightColor("High Light Color", Color) = (1,1,1,1)
        _HightLightValue("Hight Light Value", Range(0, 2)) = 1
		_Color("Color", Color) = (1,1,1,1)
        _Cutoff( "Mask Clip Value", Float ) = 0.5
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_BlurSize("BlurSize", float) = 10
		_SeeThroughOpacity("SeeThroughOpacityAdjust", Range(0,1)) = 0.5
	}

		SubShader
		{
			// Regular Pass
			Tags{ "RenderType" = "TransparentCutout" }
            Cull Off
			LOD 200

			CGPROGRAM
			#pragma target 3.0
		    #pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vert 


			sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
            uniform float _Cutoff = 0.5;

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

            void vert( inout appdata_full v, out Input o )
            {
                UNITY_INITIALIZE_OUTPUT( Input, o );
                //Calculate new billboard vertex position and normal;
                float3 upCamVec = normalize ( UNITY_MATRIX_V._m10_m11_m12 );
                float3 forwardCamVec = -normalize ( UNITY_MATRIX_V._m20_m21_m22 );
                float3 rightCamVec = normalize( UNITY_MATRIX_V._m00_m01_m02 );
                float4x4 rotationCamMatrix = float4x4( rightCamVec, 0, upCamVec, 0, forwardCamVec, 0, 0, 0, 0, 1 );
                v.normal = normalize( mul( float4( v.normal , 0 ), rotationCamMatrix )).xyz;
                //This unfortunately must be made to take non-uniform scaling into account;
                //Transform to world coords, apply rotation and transform back to local;
                v.vertex = mul( v.vertex , unity_ObjectToWorld );
                v.vertex = mul( v.vertex , rotationCamMatrix );
                v.vertex = mul( v.vertex , unity_WorldToObject );
                float3 ase_vertex3Pos = v.vertex.xyz;
                v.vertex.xyz += ( 0 + ( ase_vertex3Pos * 1 ) );
                v.vertex.w = 1;
            }

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				// Metallic and smoothness come from slider variables
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Alpha = c.a;
                clip( c.a - _Cutoff );
			}
			ENDCG

			// see through pass

			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200
			Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency

			ZWrite Off
			ZTest Greater

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard alpha:fade addshadow fullforwardshadows vertex:vert 
			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			float _BlurSize;
			float _SeeThroughOpacity;
            float _HightLightValue = 1;

			struct Input
			{
				float2 uv_MainTex;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			fixed4 _HighLightColor;

			UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
            UNITY_INSTANCING_BUFFER_END(Props)

            void vert( inout appdata_full v, out Input o )
            {
                UNITY_INITIALIZE_OUTPUT( Input, o );
                //Calculate new billboard vertex position and normal;
                float3 upCamVec = normalize ( UNITY_MATRIX_V._m10_m11_m12 );
                float3 forwardCamVec = -normalize ( UNITY_MATRIX_V._m20_m21_m22 );
                float3 rightCamVec = normalize( UNITY_MATRIX_V._m00_m01_m02 );
                float4x4 rotationCamMatrix = float4x4( rightCamVec, 0, upCamVec, 0, forwardCamVec, 0, 0, 0, 0, 1 );
                v.normal = normalize( mul( float4( v.normal , 0 ), rotationCamMatrix )).xyz;
                //This unfortunately must be made to take non-uniform scaling into account;
                //Transform to world coords, apply rotation and transform back to local;
                v.vertex = mul( v.vertex , unity_ObjectToWorld );
                v.vertex = mul( v.vertex , rotationCamMatrix );
                v.vertex = mul( v.vertex , unity_WorldToObject );
                float3 ase_vertex3Pos = v.vertex.xyz;
                v.vertex.xyz += ( 0 + ( ase_vertex3Pos * 1 ) );
                v.vertex.w = 1;
            }

            void surf(Input IN, inout SurfaceOutputStandard o)
            {
				// small blur effect on the texture
				fixed4 cR = tex2D(_MainTex, float2(IN.uv_MainTex.x + _MainTex_TexelSize.x * _BlurSize,IN.uv_MainTex.y));
				fixed4 cL = tex2D(_MainTex, float2(IN.uv_MainTex.x - _MainTex_TexelSize.x * _BlurSize,IN.uv_MainTex.y));
				fixed4 cT = tex2D(_MainTex, float2(IN.uv_MainTex.x,IN.uv_MainTex.y + _MainTex_TexelSize.y * _BlurSize));
				fixed4 cB = tex2D(_MainTex, float2(IN.uv_MainTex.x,IN.uv_MainTex.y - _MainTex_TexelSize.y * _BlurSize));
                fixed4 cResult = (cR + cL + cT + cB) ;
				fixed4 c = cResult * 0.015 + _HighLightColor * _HightLightValue;
                c.a = cResult.a;
				// fixed4 c = (cR + cL + cT + cB) * _HighLightColor * _HightLightValue;
				o.Albedo = c;
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				// make object also bit transparent
				o.Alpha = c.a * _SeeThroughOpacity;
			}
			ENDCG

		} // subshader
    FallBack "Diffuse"
}
