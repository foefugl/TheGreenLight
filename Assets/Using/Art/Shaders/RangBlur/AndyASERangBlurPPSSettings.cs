// Amplify Shader Editor - Visual Shader Editing Tool
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>
#if UNITY_POST_PROCESSING_STACK_V2
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess( typeof( AndyASERangBlurPPSRenderer ), PostProcessEvent.AfterStack, "AndyASERangBlur", true )]
public sealed class AndyASERangBlurPPSSettings : PostProcessEffectSettings
{
	[Tooltip( "Value" )]
	public FloatParameter _Value = new FloatParameter { value = 0.004f };
	[Tooltip( "Dot" )]
	public TextureParameter _Dot = new TextureParameter {  };
	[Tooltip( "Focus" )]
	public FloatParameter _Focus = new FloatParameter { value = -1f };
	[Tooltip( "WholeBulrLerp" )]
	public FloatParameter _WholeBulrLerp = new FloatParameter { value = 0f };
}

public sealed class AndyASERangBlurPPSRenderer : PostProcessEffectRenderer<AndyASERangBlurPPSSettings>
{
	public override void Render( PostProcessRenderContext context )
	{
		var sheet = context.propertySheets.Get( Shader.Find( "AndyASE/RangBlur" ) );
		sheet.properties.SetFloat( "_Value", settings._Value );
		if(settings._Dot.value != null) sheet.properties.SetTexture( "_Dot", settings._Dot );
		sheet.properties.SetFloat( "_Focus", settings._Focus );
		sheet.properties.SetFloat( "_WholeBulrLerp", settings._WholeBulrLerp );
		context.command.BlitFullscreenTriangle( context.source, context.destination, sheet, 0 );
	}
}
#endif
