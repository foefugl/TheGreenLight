// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyASE/SpriteMeshAlphaCut"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.65
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Columns("Columns", Int) = 0
		_Rows("Rows", Int) = 0
		_Frame("Frame", Int) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" }
		Cull Off
		Stencil
		{
			Ref 2
			CompFront Always
			PassFront Replace
			ZFailFront DecrWrap
		}
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform int _Columns;
		uniform int _Rows;
		uniform int _Frame;
		uniform float _Cutoff = 0.65;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles2 = (float)_Columns * (float)_Rows;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset2 = 1.0f / (float)_Columns;
			float fbrowsoffset2 = 1.0f / (float)_Rows;
			// Speed of animation
			float fbspeed2 = _Time[ 1 ] * 0.0;
			// UV Tiling (col and row offset)
			float2 fbtiling2 = float2(fbcolsoffset2, fbrowsoffset2);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			float fbcurrenttileindex2 = round( fmod( fbspeed2 + (float)_Frame, fbtotaltiles2) );
			fbcurrenttileindex2 += ( fbcurrenttileindex2 < 0) ? fbtotaltiles2 : 0;
			// Obtain Offset X coordinate from current tile linear index
			float fblinearindextox2 = round ( fmod ( fbcurrenttileindex2, (float)_Columns ) );
			// Multiply Offset X by coloffset
			float fboffsetx2 = fblinearindextox2 * fbcolsoffset2;
			// Obtain Offset Y coordinate from current tile linear index
			float fblinearindextoy2 = round( fmod( ( fbcurrenttileindex2 - fblinearindextox2 ) / (float)_Columns, (float)_Rows ) );
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy2 = (int)((float)_Rows-1) - fblinearindextoy2;
			// Multiply Offset Y by rowoffset
			float fboffsety2 = fblinearindextoy2 * fbrowsoffset2;
			// UV Offset
			float2 fboffset2 = float2(fboffsetx2, fboffsety2);
			// Flipbook UV
			half2 fbuv2 = i.uv_texcoord * fbtiling2 + fboffset2;
			// *** END Flipbook UV Animation vars ***
			float4 tex2DNode1 = tex2D( _TextureSample0, fbuv2 );
			o.Albedo = tex2DNode1.rgb;
			o.Alpha = 1;
			clip( tex2DNode1.a - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
0;73;1920;946;1262;274;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;3;-844,5.5;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IntNode;4;-842,125.5;Inherit;False;Property;_Columns;Columns;2;0;Create;True;0;0;False;0;False;0;1;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;5;-843,202.5;Inherit;False;Property;_Rows;Rows;3;0;Create;True;0;0;False;0;False;0;1;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;6;-842,288.5;Inherit;False;Property;_Frame;Frame;4;0;Create;True;0;0;False;0;False;0;1;0;1;INT;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;2;-616,9.5;Inherit;False;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SamplerNode;1;-382,4.5;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;False;-1;aec5070f5cb24b24e9fbdf06b0d1faf8;dfaa085003012d54f8c2b3abf27ae83c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;7;-266,201;Inherit;False;Property;_Opacity;Opacity;5;0;Create;True;0;0;False;0;False;0;-0.93;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;87,-7;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;AndyASE/SpriteMeshAlphaCut;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.65;True;True;0;True;TransparentCutout;;Geometry;All;14;all;True;True;True;True;0;False;-1;True;2;False;-1;255;False;-1;255;False;-1;7;False;-1;3;False;-1;0;False;-1;8;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;3;0
WireConnection;2;1;4;0
WireConnection;2;2;5;0
WireConnection;2;4;6;0
WireConnection;1;1;2;0
WireConnection;0;0;1;0
WireConnection;0;10;1;4
ASEEND*/
//CHKSM=8160131E5686FB30290C5CF78DFD76F49EFF7586