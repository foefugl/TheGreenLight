// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyASE/FogOfWar"
{
	Properties
	{
		_Value3("Value", Range( 0 , 0.004)) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_SketchSpeed("SketchSpeed", Float) = 0
		_SketchTiling("SketchTiling", Float) = 0
		_SketchValue("SketchValue", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		Stencil
		{
			Ref 2
			CompFront Always
			PassFront Replace
			ZFailFront DecrWrap
		}
		GrabPass{ }
		GrabPass{ "_GrabScreen2" }
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#if defined(UNITY_STEREO_INSTANCING_ENABLED) || defined(UNITY_STEREO_MULTIVIEW_ENABLED)
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex);
		#else
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex)
		#endif
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float4 screenPos;
		};

		ASE_DECLARE_SCREENSPACE_TEXTURE( _GrabTexture )
		uniform float _Value3;
		ASE_DECLARE_SCREENSPACE_TEXTURE( _GrabScreen2 )
		uniform float _SketchValue;
		uniform sampler2D _TextureSample0;
		uniform float _SketchSpeed;
		uniform float _SketchTiling;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float2 appendResult13 = (float2(ase_screenPos.x , ase_screenPos.y));
			float2 ScreenUV37 = appendResult13;
			float Value2 = _Value3;
			float4 screenColor32 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,(ScreenUV37*1.0 + Value2));
			float2 appendResult18 = (float2(Value2 , 0.0));
			float4 screenColor26 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,(ScreenUV37*1.0 + appendResult18));
			float2 appendResult12 = (float2(0.0 , Value2));
			float4 screenColor20 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabScreen2,(ScreenUV37*1.0 + appendResult12));
			float temp_output_4_0 = ( Value2 * -1.0 );
			float4 screenColor28 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,(ScreenUV37*1.0 + temp_output_4_0));
			float2 appendResult10 = (float2(temp_output_4_0 , 0.0));
			float4 screenColor27 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,(ScreenUV37*1.0 + appendResult10));
			float2 appendResult11 = (float2(0.0 , temp_output_4_0));
			float4 screenColor29 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,(ScreenUV37*1.0 + appendResult11));
			float grayscale34 = Luminance(( ( ( screenColor32 + screenColor26 + screenColor20 ) + ( screenColor28 + screenColor27 + screenColor29 ) ) / 6.0 ).rgb);
			float2 temp_cast_1 = (_SketchSpeed).xx;
			float2 panner41 = ( 1.0 * _Time.y * temp_cast_1 + ( ScreenUV37 * _SketchTiling ));
			o.Emission = ( grayscale34 * CalculateContrast(_SketchValue,tex2D( _TextureSample0, panner41 )) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
0;7;1920;1012;1077.532;74.8026;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;19;-2027.577,234.2033;Inherit;False;Property;_Value3;Value;1;0;Create;True;0;0;False;0;False;0;0.003;0;0.004;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2;-1749.378,235.5035;Inherit;False;Value;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;5;-1871.978,33.10281;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;9;-1988.578,849.1029;Inherit;False;2;Value;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1993.778,977.8024;Inherit;False;Constant;_Float9;Float 2;3;0;Create;True;0;0;False;0;False;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;13;-1673.377,66.10295;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-1711.678,888.1026;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-1679.178,326.5033;Inherit;False;Constant;_Float11;Float 0;2;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-1719.385,984.9425;Inherit;False;Constant;_Float10;Float 0;2;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;37;-1529.006,80.5909;Inherit;False;ScreenUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;39;-1578.402,714.8523;Inherit;False;37;ScreenUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;10;-1515.285,918.6426;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-1476.378,282.3031;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;11;-1519.184,1039.543;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;12;-1480.278,403.2032;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;31;-1309.276,1036.552;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;30;-1296.978,79.50301;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;17;-1270.369,400.213;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;8;-1319.77,253.313;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;7;-1313.676,891.6525;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;25;-1335.885,715.8427;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenColorNode;29;-1099.893,1064.632;Inherit;False;Global;_GrabScreen5;Grab Screen 5;1;0;Create;True;0;0;False;0;False;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;27;-1094.693,882.6326;Inherit;False;Global;_GrabScreen4;Grab Screen 4;1;0;Create;True;0;0;False;0;False;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;20;-1060.986,428.2931;Inherit;False;Global;_GrabScreen2;Grab Screen 2;1;0;Create;True;0;0;False;0;False;Object;-1;True;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;26;-1055.786,246.2932;Inherit;False;Global;_GrabScreen1;Grab Screen 1;1;0;Create;True;0;0;False;0;False;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;38;-798.4021,357.8523;Inherit;False;37;ScreenUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenColorNode;28;-1095.301,708.8229;Inherit;False;Global;_GrabScreen3;Grab Screen 3;1;0;Create;True;0;0;False;0;False;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;43;-791.532,432.1974;Inherit;False;Property;_SketchTiling;SketchTiling;4;0;Create;True;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;32;-1056.394,72.48329;Inherit;False;Global;_GrabScreen0;Grab Screen 0;1;0;Create;True;0;0;False;0;False;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-623.532,362.1974;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-791.532,508.1974;Inherit;False;Property;_SketchSpeed;SketchSpeed;3;0;Create;True;0;0;False;0;False;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;23;-828.8841,791.242;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-789.9775,154.9028;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PannerNode;41;-495.5325,379.1974;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-569.6536,249.2063;Inherit;False;Constant;_Float12;Float 3;2;0;Create;True;0;0;False;0;False;6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;22;-569.6588,124.1987;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;33;-342.9098,202.1232;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;35;-311.2528,363.6345;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;False;-1;None;77d91a067048a684691c98b3a50aad8d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;48;-183.532,558.1974;Inherit;False;Property;_SketchValue;SketchValue;5;0;Create;True;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;47;-4.531982,423.1974;Inherit;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCGrayscale;34;-112.4532,186.3643;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;200.7472,179.6345;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;496,108;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;AndyASE/FogOfWar;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Opaque;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;True;2;False;-1;255;False;-1;255;False;-1;7;False;-1;3;False;-1;0;False;-1;8;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;1;False;-1;1;False;-1;0;0;False;-1;0;False;-1;1;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;19;0
WireConnection;13;0;5;1
WireConnection;13;1;5;2
WireConnection;4;0;9;0
WireConnection;4;1;3;0
WireConnection;37;0;13;0
WireConnection;10;0;4;0
WireConnection;10;1;14;0
WireConnection;18;0;2;0
WireConnection;18;1;15;0
WireConnection;11;0;14;0
WireConnection;11;1;4;0
WireConnection;12;0;15;0
WireConnection;12;1;2;0
WireConnection;31;0;39;0
WireConnection;31;2;11;0
WireConnection;30;0;37;0
WireConnection;30;2;2;0
WireConnection;17;0;37;0
WireConnection;17;2;12;0
WireConnection;8;0;37;0
WireConnection;8;2;18;0
WireConnection;7;0;39;0
WireConnection;7;2;10;0
WireConnection;25;0;39;0
WireConnection;25;2;4;0
WireConnection;29;0;31;0
WireConnection;27;0;7;0
WireConnection;20;0;17;0
WireConnection;26;0;8;0
WireConnection;28;0;25;0
WireConnection;32;0;30;0
WireConnection;44;0;38;0
WireConnection;44;1;43;0
WireConnection;23;0;28;0
WireConnection;23;1;27;0
WireConnection;23;2;29;0
WireConnection;24;0;32;0
WireConnection;24;1;26;0
WireConnection;24;2;20;0
WireConnection;41;0;44;0
WireConnection;41;2;42;0
WireConnection;22;0;24;0
WireConnection;22;1;23;0
WireConnection;33;0;22;0
WireConnection;33;1;21;0
WireConnection;35;1;41;0
WireConnection;47;1;35;0
WireConnection;47;0;48;0
WireConnection;34;0;33;0
WireConnection;36;0;34;0
WireConnection;36;1;47;0
WireConnection;0;2;36;0
ASEEND*/
//CHKSM=806B9F609992E3CA2369890776C3776EDD314134