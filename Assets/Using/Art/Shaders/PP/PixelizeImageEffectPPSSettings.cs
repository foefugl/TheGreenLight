// Amplify Shader Editor - Visual Shader Editing Tool
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>
#if UNITY_POST_PROCESSING_STACK_V2
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess( typeof( PixelizeImageEffectPPSRenderer ), PostProcessEvent.AfterStack, "Custoom/PixelizeImageEffect", true )]
public sealed class PixelizeImageEffectPPSSettings : PostProcessEffectSettings
{
	[Tooltip( "Screen" )]
	public FloatParameter _PixelsX = new FloatParameter { value = 80f };
	[Tooltip( "Pixels X" )]
	public FloatParameter _PixelsY = new FloatParameter { value = 80f };
}

public sealed class PixelizeImageEffectPPSRenderer : PostProcessEffectRenderer<PixelizeImageEffectPPSSettings>
{
	public override void Render( PostProcessRenderContext context )
	{
		var sheet = context.propertySheets.Get( Shader.Find( "PixelizeImageEffect" ) );
		sheet.properties.SetFloat( "_PixelsX", settings._PixelsX );
		sheet.properties.SetFloat( "_PixelsY", settings._PixelsY );
		context.command.BlitFullscreenTriangle( context.source, context.destination, sheet, 0 );
	}
}
#endif
