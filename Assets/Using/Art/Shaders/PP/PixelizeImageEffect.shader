// Upgrade NOTE: upgraded instancing buffer 'PixelizeImageEffect' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PixelizeImageEffect"
{
	Properties
	{
		_PixelsX("Pixels X", Float) = 80
		_PixelsY("Pixels Y", Float) = 80

	}

	SubShader
	{
		LOD 0

		Cull Off
		ZWrite Off
		ZTest Always
		
		Pass
		{
			CGPROGRAM

			

			#pragma vertex Vert
			#pragma fragment Frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#pragma multi_compile_instancing

		
			struct ASEAttributesDefault
			{
				float3 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				
			};

			struct ASEVaryingsDefault
			{
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoordStereo : TEXCOORD1;
			#if STEREO_INSTANCING_ENABLED
				uint stereoTargetEyeIndex : SV_RenderTargetArrayIndex;
			#endif
				
			};

			uniform sampler2D _MainTex;
			uniform half4 _MainTex_TexelSize;
			uniform half4 _MainTex_ST;
			
			UNITY_INSTANCING_BUFFER_START(PixelizeImageEffect)
				UNITY_DEFINE_INSTANCED_PROP(float, _PixelsX)
#define _PixelsX_arr PixelizeImageEffect
				UNITY_DEFINE_INSTANCED_PROP(float, _PixelsY)
#define _PixelsY_arr PixelizeImageEffect
			UNITY_INSTANCING_BUFFER_END(PixelizeImageEffect)


			
			float2 TransformTriangleVertexToUV (float2 vertex)
			{
				float2 uv = (vertex + 1.0) * 0.5;
				return uv;
			}

			ASEVaryingsDefault Vert( ASEAttributesDefault v  )
			{
				ASEVaryingsDefault o;
				o.vertex = float4(v.vertex.xy, 0.0, 1.0);
				o.texcoord = TransformTriangleVertexToUV (v.vertex.xy);
#if UNITY_UV_STARTS_AT_TOP
				o.texcoord = o.texcoord * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
				o.texcoordStereo = TransformStereoScreenSpaceTex (o.texcoord, 1.0);

				v.texcoord = o.texcoordStereo;
				float4 ase_ppsScreenPosVertexNorm = float4(o.texcoordStereo,0,1);

				

				return o;
			}

			float4 Frag (ASEVaryingsDefault i  ) : SV_Target
			{
				float4 ase_ppsScreenPosFragNorm = float4(i.texcoordStereo,0,1);

				float2 texCoord10 = i.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float _PixelsX_Instance = UNITY_ACCESS_INSTANCED_PROP(_PixelsX_arr, _PixelsX);
				float _PixelsY_Instance = UNITY_ACCESS_INSTANCED_PROP(_PixelsY_arr, _PixelsY);
				float pixelWidth4 =  1.0f / _PixelsX_Instance;
				float pixelHeight4 = 1.0f / _PixelsY_Instance;
				half2 pixelateduv4 = half2((int)(texCoord10.x / pixelWidth4) * pixelWidth4, (int)(texCoord10.y / pixelHeight4) * pixelHeight4);
				

				float4 color = tex2D( _MainTex, pixelateduv4 );
				
				return color;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18500
0;7;1920;1012;1443.164;563.024;1.019996;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;10;-594.7116,-52.36871;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;8;-523.9514,143.5614;Inherit;False;InstancedProperty;_PixelsY;Pixels Y;1;0;Create;True;0;0;False;0;False;80;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-524.9513,62.56125;Inherit;False;InstancedProperty;_PixelsX;Pixels X;0;0;Create;True;0;0;False;0;False;80;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCPixelate;4;-374.8218,-50.39874;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;14;-330.3487,-128.5058;Inherit;False;0;0;_MainTex;Pass;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;6;-164.8007,-70.15992;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;13;141.1497,-74.19996;Float;False;True;-1;2;ASEMaterialInspector;0;2;PixelizeImageEffect;32139be9c1eb75640a847f011acf3bcf;True;SubShader 0 Pass 0;0;0;SubShader 0 Pass 0;1;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;True;2;False;-1;True;7;False;-1;False;False;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;4;0;10;0
WireConnection;4;1;7;0
WireConnection;4;2;8;0
WireConnection;6;0;14;0
WireConnection;6;1;4;0
WireConnection;13;0;6;0
ASEEND*/
//CHKSM=B479448B12A2C7E383A6D7111511BE8AEFBD6F51