﻿Shader "Unlit/BillboardLight"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Color ("BaseColor", Color) = (1, 1, 1, 1)
        _SpecColor ("Specular Color", Color) = (.5, .5, .5, 1)
        _Shininess ("Shininess", float) = 10
        _RimColor ("Rim Color", Color) = (1, 1, 1, 1)
        _RimPower ("Rim Power", Range(.1, 10)) = 5
    }
    SubShader
    {
        ZWrite On
        Cull Off
        
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            Tags 
            {
                "LightMode" = "ForwardBase"
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            // Diffuse
            uniform float4 _Color;
            // Specular
            uniform float4 _SpecColor;
            uniform float  _Shininess;
            // Rim
            uniform float4 _RimColor;
            uniform float  _RimPower;

            // unity variables
            uniform float4 _LightColor0;

            // input structs
            struct vertexInput{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };
            struct vertexOutput{
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 posWorld  : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float color : COLOR; 
                float4 diff : COLOR1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float rayPlaneIntersection( float3 rayDir, float3 rayPos, float3 planeNormal, float3 planePos)
            {
                float denom = dot(planeNormal, rayDir);
                denom = max(denom, 0.000001); // avoid divide by zero
                float3 diff = planePos - rayPos;
                return dot(diff, planeNormal) / denom;
            }

            vertexOutput vert(vertexInput v)
            {
                vertexOutput o;
                o.uv = v.uv.xy;

                // billboard mesh towards camera
                float3 vpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
                float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13, unity_ObjectToWorld._m23, 1);
                float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
 
                o.pos = mul(UNITY_MATRIX_P, viewPos);
 
                // calculate distance to vertical billboard plane seen at this vertex's screen position
                float3 planeNormal = normalize(float3(UNITY_MATRIX_V._m20, 0.0, UNITY_MATRIX_V._m22));
                float3 planePoint = unity_ObjectToWorld._m03_m13_m23;
                float3 rayStart = _WorldSpaceCameraPos.xyz;
                float3 rayDir = -normalize(mul(UNITY_MATRIX_I_V, float4(viewPos.xyz, 1.0)).xyz - rayStart); // convert view to world, minus camera pos
                float dist = rayPlaneIntersection(rayDir, rayStart, planeNormal, planePoint);
 
                // calculate the clip space z for vertical plane
                float4 planeOutPos = mul(UNITY_MATRIX_VP, float4(rayStart + rayDir * dist, 1.0));
                float newPosZ = planeOutPos.z / planeOutPos.w * o.pos.w;
 
                // use the closest clip space z
                #if defined(UNITY_REVERSED_Z)
                o.pos.z = max(o.pos.z, newPosZ);
                #else
                o.pos.z = min(o.pos.z, newPosZ);
                #endif
 
                // UNITY_TRANSFER_FOG(o,o.vertex);

                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
                o.color = _Color;

                // only evaluate ambient
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                o.diff.rgb = ShadeSH9(half4(worldNormal,1));
                o.diff.a = 1;

                return o;
            }

            // fragment function
            float4 frag(vertexOutput i) : SV_Target
            {
                float3 normalDirection = i.normalDir;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection;
                float atten;

                // check light type
                if(_WorldSpaceLightPos0.w == 0) // this is directional light
                {
                    lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                    atten = 1;
                }
                else
                {
                    float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
                    float distance = length(fragmentToLightSource);
                    lightDirection = normalize(fragmentToLightSource);
                    atten = 1/distance;
                }
                // diffuse
                float3 diffuseReflection = atten * _LightColor0.xyz * _Color * saturate(dot(normalDirection, lightDirection));
                // specular
                float3 specularReflection = atten * _LightColor0.xyz * _SpecColor * saturate(dot(normalDirection, lightDirection)) 
                * pow(max(0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
                // rim
                float rim = 1 - saturate(dot(viewDirection, normalDirection));
                float3 rimLight = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection)) * pow(rim, _RimPower);
                float4 finalColor = float4(diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT, 1);
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *= finalColor;
                return col;
            }
            ENDCG
        }
        Pass
        {
            Tags {
                "LightMode" = "ForwardAdd"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            //pragmas
            #pragma vertex vert
            #pragma fragment frag
       
            //users variables
            ////diffuse
            uniform float4 _Color;
            ////specular
            uniform float4 _SpecColor;
            uniform float _Shininess;
            ////rim
            uniform float4 _RimColor;
            uniform float _RimPower;
 
            //unity variables
            uniform float4 _LightColor0;
 
            //input structs
            struct vertexInput{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };
            struct vertexOutput{
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 col : COLOR;
            };

            float rayPlaneIntersection( float3 rayDir, float3 rayPos, float3 planeNormal, float3 planePos)
            {
                float denom = dot(planeNormal, rayDir);
                denom = max(denom, 0.000001); // avoid divide by zero
                float3 diff = planePos - rayPos;
                return dot(diff, planeNormal) / denom;
            }
 
            sampler2D _MainTex;
            float4 _MainTex_ST;

            //vertex function
            vertexOutput vert(vertexInput v){
                vertexOutput o;
                o.uv = v.uv.xy;
                // billboard mesh towards camera
                float3 vpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
                float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13, unity_ObjectToWorld._m23, 1);
                float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
 
                o.pos = mul(UNITY_MATRIX_P, viewPos);
 
                // calculate distance to vertical billboard plane seen at this vertex's screen position
                float3 planeNormal = normalize(float3(UNITY_MATRIX_V._m20, 0.0, UNITY_MATRIX_V._m22));
                float3 planePoint = unity_ObjectToWorld._m03_m13_m23;
                float3 rayStart = _WorldSpaceCameraPos.xyz;
                float3 rayDir = -normalize(mul(UNITY_MATRIX_I_V, float4(viewPos.xyz, 1.0)).xyz - rayStart); // convert view to world, minus camera pos
                float dist = rayPlaneIntersection(rayDir, rayStart, planeNormal, planePoint);
 
                // calculate the clip space z for vertical plane
                float4 planeOutPos = mul(UNITY_MATRIX_VP, float4(rayStart + rayDir * dist, 1.0));
                float newPosZ = planeOutPos.z / planeOutPos.w * o.pos.w;
 
                // use the closest clip space z
                #if defined(UNITY_REVERSED_Z)
                o.pos.z = max(o.pos.z, newPosZ);
                #else
                o.pos.z = min(o.pos.z, newPosZ);
                #endif
 
                // UNITY_TRANSFER_FOG(o,o.vertex);

                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.normalDir = normalize(mul(float4(v.normal, 0),unity_WorldToObject).xyz);
               
                o.col = _Color;
               
                return o;
               
            }
 
            //fragment function
            float4 frag(vertexOutput i) : SV_Target{
                float3 normalDirection = i.normalDir;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection;
                float atten;
               
                //check light type
                if(_WorldSpaceLightPos0.w == 0){ // this is directional light
                    lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                    atten = 1;
                }else{
                    float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
                    float distance = length(fragmentToLightSource);
                    lightDirection = normalize(fragmentToLightSource);
                    atten = 1/distance;
                }

               
                //diffuse
                float3 diffuseReflection = atten * _LightColor0.xyz * _Color * saturate(dot(normalDirection, lightDirection));
               
                //specular
                float3 specularReflection = atten * _LightColor0.xyz * _SpecColor * saturate(dot(normalDirection, lightDirection)) * pow(max(0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
               
                //rim
                float rim = 1 - saturate(dot(viewDirection, normalDirection));
                float3 rimLight = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection)) * pow(rim, _RimPower);
                
                //final color
                float4 finalColor = float4(diffuseReflection + specularReflection + rimLight, 1);
                // float4 finalColor = float4(diffuseReflection, 1);
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *= finalColor;
                return col;
            }
            ENDCG
        }//pass
    }
}
