﻿Shader "Unlit/ForwardRenderingMat"
{
    Properties
    {
        _Diffuse("Diffuse",Color)=(1,1,1,1)
        _Specular("Specular",Color)=(1,1,1,1)
        _Gloss("Gloss",Range(8.0,256))=20
    }

    SubShader
    {
        Pass //第一个Pass使用Base用来逐像素平行光光照
        {
            Tags{"LightModel"="ForwardBase"}
            CGPROGRAM
            #include "Lighting.cginc"
            #pragma vertex vert
            #pragma fragment frag

            fixed4 _Diffuse;
            fixed4 _Specular;
            float _Gloss;

            struct a2v
            {
                float4 vertex:POSITION;
                float3 normal:NORMAL;
            };

            struct v2f
            {
                float4 pos:SV_POSITION;
                float3 worldPos:TEXCOORD0;
                float3 worldNormal:TEXCOORD1;
            };

            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex).xyz;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);

                return o;
            }

            fixed4 frag(v2f i):SV_TARGET
            {
                float3 worldPos = i.worldPos;
                float3 worldNormal = normalize(i.worldNormal);
                float3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
                float3 worldViewDir = normalize(_WorldSpaceCameraPos.xyz-i.worldPos.xyz);
                float3 halfNormal = normalize(worldLightDir+worldViewDir);

                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz;  
                //_LightColor0已经是颜色和强度相乘之后的结果了
                fixed3 diffuse = _LightColor0.rgb * _Diffuse.rgb * saturate(dot(worldNormal,worldLightDir));
                fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(saturate(dot(worldNormal,halfNormal)),_Gloss);

                fixed atten = 1.0; //光的衰减，平行光是没有衰减的
                
                return fixed4(ambient+(diffuse+specular)*atten,1.0);
            }

            ENDCG
        }
        
        Pass  //第二个Pass用来计算其他光源的逐像素光照，可能是平行光，也可能是点光源，聚光灯
        {
            Tags{"LightModel"="ForwardAdd"}
            Blend One One

            CGPROGRAM
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            //为了得到正确的衰减因子
          //  #pragma multi_compile_fwadd  
            #pragma multi_compile_fwadd_fullshadows
            #pragma vertex vert
            #pragma fragment frag
            
            fixed4 _Diffuse;
            fixed4 _Specular;
            float _Gloss;

            struct a2v
            {
                float4 vertex:POSITION;
                float3 normal:NORMAL;
            };

            struct v2f
            {
                float4 pos:SV_POSITION;
                float3 worldPos:TEXCOORD0;
                float3 worldNormal:TEXCOORD1;
            };

            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex).xyz;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);

                return o;
            }

            fixed4 frag(v2f i):SV_TARGET
            {
                fixed3 worldPos = i.worldPos;
                fixed3 worldNormal = normalize(i.worldNormal);

                #ifdef USING_DIRECTIONAL_LIGHT
                    fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
                #else
                    fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz-i.worldPos);
                #endif
                fixed3 worldViewDir = normalize(_WorldSpaceCameraPos.xyz-i.worldPos);
                fixed3 halfNormal = normalize(worldViewDir+worldLightDir);

                //因为已经在第一个Pass中计算了环境光，所以这里就不需要再次计算了
                fixed3 diffuse = _LightColor0.rgb * _Diffuse.rgb * saturate(dot(worldNormal,worldLightDir));
                fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(saturate(dot(worldNormal,halfNormal)),_Gloss);
                
                #ifdef USING_DIRECTIONAL_LIGHT
                    fixed atten = 1.0;
                #else
                    //Unity提供了衰减因子的查找表，并存在了一个图片中，我们只要采样就可以
                    //但这样也会有精度的问题
                    #if defined(POINT)
                        float3 lightCoord = mul(unity_WorldToLight,float4(i.worldPos,1)).xyz;
                        //A.rr操作相当于是取了两个一样的值（A,A）
                        fixed atten = tex2D(_LightTexture0,dot(lightCoord,lightCoord).rr).UNITY_ATTEN_CHANNEL;
                    #elif defined(SPOT)
                        float4 lightCoord = mul(unity_WorldToLight, float4(i.worldPos, 1));
                        fixed atten = (lightCoord.z > 0) * tex2D(_LightTexture0, lightCoord.xy / lightCoord.w + 0.5).w * tex2D(_LightTextureB0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL; 
                    #else
                        fixed atten = 1.0;
                    #endif
                #endif

                return fixed4((diffuse+specular)*atten,1.0);
            }

            ENDCG
        }
    }
    
}
