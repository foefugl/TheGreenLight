﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/PointLightCustomShader"
{
    Properties
    {
        _Color ("BaseColor", Color) = (1, 1, 1, 1)
        _SpecColor ("Specular Color", Color) = (.5, .5, .5, 1)
        _Shininess ("Shininess", float) = 10
        _RimColor ("Rim Color", Color) = (1, 1, 1, 1)
        _RimPower ("Rim Power", Range(.1, 10)) = 5
    }
    SubShader
    {
        Pass
        {
            Tags 
            {
                "LightMode" = "ForwardBase"
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            // Diffuse
            uniform float4 _Color;
            // Specular
            uniform float4 _SpecColor;
            uniform float  _Shininess;
            // Rim
            uniform float4 _RimColor;
            uniform float  _RimPower;

            // unity variables
            uniform float4 _LightColor0;

            // input structs
            struct vertexInput{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct vertexOutput{
                float4 pos : SV_POSITION;
                float4 posWorld  : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float color : COLOR; 
            };

            vertexOutput vert(vertexInput v)
            {
                vertexOutput o;

                o.pos = UnityObjectToClipPos(v.vertex);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);

                o.color = _Color;

                return o;
            }

            // fragment function
            float4 frag(vertexOutput i) : COLOR
            {
                float3 normalDirection = i.normalDir;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection;
                float atten;

                // check light type
                if(_WorldSpaceLightPos0.w == 0) // this is directional light
                {
                    lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                    atten = 1;
                }
                else
                {
                    float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
                    float distance = length(fragmentToLightSource);
                    lightDirection = normalize(fragmentToLightSource);
                    atten = 1/distance;
                }
                // diffuse
                float3 diffuseReflection = atten * _LightColor0.xyz * _Color * saturate(dot(normalDirection, lightDirection));
                // specular
                float3 specularReflection = atten * _LightColor0.xyz * _SpecColor * saturate(dot(normalDirection, lightDirection)) 
                * pow(max(0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
                // rim
                float rim = 1 - saturate(dot(viewDirection, normalDirection));
                float3 rimLight = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection)) * pow(rim, _RimPower);
                float4 finalColor = float4(diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT, 1);
                return finalColor;
            }
            ENDCG
        }
        Pass
        {
            Tags {
                "LightMode" = "ForwardAdd"
            }
            Blend One One
            CGPROGRAM
            //pragmas
            #pragma vertex vert
            #pragma fragment frag
       
            //users variables
            ////diffuse
            uniform float4 _Color;
            ////specular
            uniform float4 _SpecColor;
            uniform float _Shininess;
            ////rim
            uniform float4 _RimColor;
            uniform float _RimPower;
 
            //unity variables
            uniform float4 _LightColor0;
 
            //input structs
            struct vertexInput{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct vertexOutput{
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 col : COLOR;
            };
 
            //vertex function
            vertexOutput vert(vertexInput v){
                vertexOutput o;
               
                o.pos = UnityObjectToClipPos(v.vertex);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                //o.normalDir = normalize(mul(float4(v.normal, 0), _World2Object).xyz);
                o.normalDir = normalize(mul(float4(v.normal, 0),unity_WorldToObject).xyz);
               
                o.col = _Color;
               
                return o;
               
            }
 
            //fragment function
            float4 frag(vertexOutput i) : COLOR{
                float3 normalDirection = i.normalDir;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection;
                float atten;
               
                //check light type
                if(_WorldSpaceLightPos0.w == 0){ // this is directional light
                    lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                    atten = 1;
                }else{
                    float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
                    float distance = length(fragmentToLightSource);
                    lightDirection = normalize(fragmentToLightSource);
                    atten = 1/distance;
                }

               
                //diffuse
                float3 diffuseReflection = atten * _LightColor0.xyz * _Color * saturate(dot(normalDirection, lightDirection));
               
                //specular
                float3 specularReflection = atten * _LightColor0.xyz * _SpecColor * saturate(dot(normalDirection, lightDirection)) * pow(max(0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
               
                //rim
                float rim = 1 - saturate(dot(viewDirection, normalDirection));
                float3 rimLight = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection)) * pow(rim, _RimPower);
                
                //final color
                float4 finalColor = float4(diffuseReflection + specularReflection + rimLight, 1);
                // float4 finalColor = float4(diffuseReflection, 1);
                return finalColor;
            }
            ENDCG
        }//pass
    }
}
