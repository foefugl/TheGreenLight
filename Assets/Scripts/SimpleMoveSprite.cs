﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlayerDataBase))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class SimpleMoveSprite : MonoBehaviour
{
    public static SimpleMoveSprite Instance { get; private set; }
    public Transform noisePosTrans;
    public VfxScriptable SmallNoise, BigNoise;
    public ParticleSystem shortAtkVfx;
    public float moveSpd = 10;
    public float chargeTime = 1.5f, chargeCounter;
    public bool IsMoving {get; private set;}
    [HideInInspector]
    public GameObject ItemUISelect;
    private bool chargeFinish, startCharge;
    private float normalSpd = 0;
    private Rigidbody rb;
    private Animator animator;
    private Noiser noiser;
    private PlayerDataBase dataBase;
    private float  lastVSpeed, lastHSpeed;
    private bool noiseCoroutinePlaying;
    private Vector3 mouseDir, mouseDirRemap;
    private void Awake() {
        if(Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        dataBase = GetComponent<PlayerDataBase>(); 
        rb       = GetComponent<Rigidbody>();
        noiser   = GetComponent<Noiser>();
        normalSpd = moveSpd;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
            mouseDir = new Vector3(hit.point.x - transform.position.x, transform.position.y, hit.point.z - transform.position.z).normalized;

        if(chargeCounter == 0){
            var hSpd = Input.GetAxisRaw("Horizontal");
            var vSpd = Input.GetAxisRaw("Vertical");
            Locomotion(hSpd, vSpd);
        }

        CruiseControl();
        ChargeFunc();
        AtkAniFunc();
        CostPlayerData();
    }
    private void ChargeFunc()
    {
        if(PlayerDataBase.Instance.InUsingItem == null)
            return;
        
        if(Input.GetMouseButtonDown(1) 
            && !dataBase.HasEnum(MotionSpeedStatus.Exhausted) 
            && !startCharge)
        {
            startCharge = true;
        }
        else if(Input.GetMouseButtonUp(1) && startCharge)
        {
            startCharge = false;
            chargeCounter = 0;
            lastHSpeed = mouseDirRemap.x;
            lastVSpeed = mouseDirRemap.z;

            if(!animator.GetCurrentAnimatorStateInfo(0).IsName("LocoMotion") || 
               !animator.GetCurrentAnimatorStateInfo(0).IsName("ShortAtk"))
            animator.CrossFade("LocoMotion", 0.005f);
        }
            

        if(startCharge)
            chargeCounter += Time.deltaTime;
        
        if(chargeCounter >= chargeTime)
            chargeFinish = true;
        else
            chargeFinish = false;

        if(chargeCounter != 0)
        {
            Vector3[] constantDir = {Vector3.forward, Vector3.back, Vector3.left, Vector3.right};
            Dictionary<Vector3, float> angleDirPair = new Dictionary<Vector3, float>();
            for(int i=0 ; i<4 ; i++){
                angleDirPair.Add(constantDir[i], Vector3.Angle(mouseDir, constantDir[i]));
            }
            Dictionary<Vector3, float> afterOrder= angleDirPair.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            mouseDirRemap = afterOrder.FirstOrDefault().Key;
            animator.SetFloat("hMouseRelativeDir", mouseDirRemap.x);
            animator.SetFloat("vMouseRelativeDir", mouseDirRemap.z);
            animator.CrossFade("ChargeMeleeMotion", 0.005f);
        }
            
        animator.SetFloat("ChargeForce", chargeCounter/chargeTime);
    }
    private void CruiseControl()
    {
        if(dataBase.Stamina.Amount <= 1 && !dataBase.HasEnum(MotionSpeedStatus.Exhausted))
            dataBase.OnlyFlagEnum(MotionSpeedStatus.Exhausted);
        if(dataBase.Stamina.Amount >= 20 && dataBase.HasEnum(MotionSpeedStatus.Exhausted))
            dataBase.RemoveEnum(MotionSpeedStatus.Exhausted);
        
        if(!dataBase.HasEnum(MotionSpeedStatus.Exhausted))
        {
            if(Input.GetKeyDown(KeyCode.LeftShift))
                dataBase.OnlyFlagEnum(MotionSpeedStatus.Run);
            else if(Input.GetKeyUp(KeyCode.LeftShift))
                dataBase.OnlyFlagEnum(MotionSpeedStatus.Walk);
        }
        
        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            dataBase.RemoveEnum(MotionSpeedStatus.Run);
            dataBase.ToggleEnum(MotionSpeedStatus.Walk);
            dataBase.ToggleEnum(MotionSpeedStatus.Creep);
        }

        if(dataBase.HasEnum(MotionSpeedStatus.Exhausted))
            moveSpd = normalSpd * 0.85f;
        else if(dataBase.HasEnum(MotionSpeedStatus.Run))
            moveSpd = normalSpd * 1.4f;
        else if(dataBase.HasEnum(MotionSpeedStatus.Creep))
            moveSpd = normalSpd * 0.65f;
        else if(dataBase.HasEnum(MotionSpeedStatus.Walk))
            moveSpd = normalSpd;
    }
    private void CostPlayerData()
    {
        if(dataBase.HasEnum(MotionSpeedStatus.Run) && rb.velocity != Vector3.zero)
            dataBase.Stamina.Amount -= 0.65f;
    }
    private void Locomotion(float _hspd, float _vspd)
    {
        if(dataBase.HasEnum(MotionSpeedStatus.Creep))
        {
            _vspd *= 0.5f;
            _hspd *= 0.5f;
        }
        
        rb.velocity = (Vector3.forward * _vspd + Vector3.right * _hspd).normalized * moveSpd;
        
        if(_hspd != 0 || _vspd != 0)
        {
            if(!noiseCoroutinePlaying)
                StartCoroutine(MotionCauseSoundVFX());
            animator.SetFloat("hSpeed", _hspd);
            animator.SetFloat("vSpeed", _vspd);
            lastHSpeed = _hspd;
            lastVSpeed = _vspd;    
            animator.SetFloat("lastHSpeed", 0);
            animator.SetFloat("lastVSpeed", 0);
            IsMoving = true;
        }
        else
        {
            animator.SetFloat("hSpeed", 0);
            animator.SetFloat("vSpeed", 0);
            animator.SetFloat("lastHSpeed", lastHSpeed);
            animator.SetFloat("lastVSpeed", lastVSpeed);
            IsMoving = false;
        }
    }
    private void AtkAniFunc()
    {
        if(PlayerDataBase.Instance.InUsingItem == null)
            return;

        if(Input.GetMouseButtonDown(0) && !dataBase.HasEnum(MotionSpeedStatus.Exhausted) && chargeFinish)
        {
            Vector3[] constantDir = {Vector3.forward, Vector3.back, Vector3.left, Vector3.right};
            Dictionary<Vector3, float> angleDirPair = new Dictionary<Vector3, float>();
            for(int i=0 ; i<4 ; i++){
                angleDirPair.Add(constantDir[i], Vector3.Angle(mouseDir, constantDir[i]));
            }
            Dictionary<Vector3, float> afterOrder= angleDirPair.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            mouseDirRemap = afterOrder.FirstOrDefault().Key;

            animator.CrossFade("ShortAtk", 0.05f);
            animator.SetFloat("hAtkDir", mouseDirRemap.x);
            animator.SetFloat("vAtkDir", mouseDirRemap.z);

            if(PlayerDataBase.Instance.InUsingItem.itemData.ObjectName == "Pipe")
            {
                shortAtkVfx.Stop();
                shortAtkVfx.Play();
            }

            dataBase.Stamina.Amount -= 15;
            lastHSpeed = mouseDirRemap.x;
            lastVSpeed = mouseDirRemap.z;
            chargeCounter = 0;

            chargeFinish = false;
            startCharge = false;
            Debug.Log("ATK's Animation Launch! ");
            if(PlayerDataBase.Instance.InUsingItem.name.Contains("Pipe"))
            {

            }
            else
            {
                PlayerDataBase.Instance.EmptyUsingItem();
            }
        }
    }
    private IEnumerator MotionCauseSoundVFX()
    {
        noiseCoroutinePlaying = true;
        Vector3 noisePos = noisePosTrans.position +　rb.velocity.normalized * 0.25f + -Camera.main.transform.forward * 5;
        if(dataBase.HasEnum(MotionSpeedStatus.Walk))
        {
            PSObjectPool.Instance.Take(SmallNoise.ID, noisePos, transform.rotation.eulerAngles, true);
            noiser.MakeSound(2.75f);
            yield return new WaitForSeconds(0.675f);
        }
        else if(dataBase.HasEnum(MotionSpeedStatus.Run))
        {
            PSObjectPool.Instance.Take(BigNoise.ID, noisePos, transform.rotation.eulerAngles, true);
            noiser.MakeSound(4f);
            yield return new WaitForSeconds(0.9f);
        }
        
        noiseCoroutinePlaying = false;
    }
}
