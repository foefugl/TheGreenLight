﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DrawMeshTest : MonoBehaviour
{
    public Material matRef;
    public MeshFilter filter;
    public Transform[] points;
    private Mesh mesh;
    private MeshRenderer rend;
    private Material mat;
    private Vector3[] vertices;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        mesh.name = "YesMesh";
        filter.mesh = mesh;
        rend = GetComponent<MeshRenderer>();
        mat = Instantiate(matRef);
        rend.material = mat;
    }

    // Update is called once per frame
    void Update()
    {
        vertices = new Vector3[points.Length];
        int[] triangles = new int[(points.Length - 2) * 3];
        vertices[0] = transform.InverseTransformPoint(points[0].position);
        vertices[1] = transform.InverseTransformPoint(points[1].position);
        vertices[2] = transform.InverseTransformPoint(points[2].position);
        vertices[3] = transform.InverseTransformPoint(points[3].position);
        Vector2[] uvs = new Vector2[vertices.Length];

        for(int i=0 ; i<uvs.Length ; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 1;
        triangles[5] = 3;

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
    }
#if UNITY_EDITOR
    private void OnDrawGizmos() 
    {
        if(Application.isPlaying)
        {
            for(int i=0 ; i<vertices.Length ; i++)
            {
                Handles.Label(vertices[i], i.ToString());
            }
        }
    }
#endif
    public Vector2 ConvertPixelsToUVCoordinates(int _x, int _y, int textureWidth, int textureHeight)
    {
        return new Vector2((float)_x / textureWidth, (float)_y / textureHeight);
    }
}
