﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowerPivot : MonoBehaviour
{
    public Transform playerRotator;
    public LayerMask camRayLayer;
    public float yAxisOffset = 0;
    public float radiusLimit = 3.75f;
    private Transform playerTransform;
    private RaycastHit hit;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;    
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, 100, camRayLayer))
        {
            Vector3 playerAdjustPos =  new Vector3(playerTransform.position.x, hit.point.y + yAxisOffset, playerTransform.position.z);
            Vector3 trackPoint = new Vector3(hit.point.x, hit.point.y + yAxisOffset, hit.point.z);
            transform.position =  playerAdjustPos + Vector3.ClampMagnitude(trackPoint - playerAdjustPos, radiusLimit);
            playerRotator.LookAt(hit.point);
        }
    }
}
