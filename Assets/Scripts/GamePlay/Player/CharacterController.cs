﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using DG.Tweening;
using UnityEngine.Experimental.GlobalIllumination;

public class CharacterController : MonoBehaviour
{
    private static CharacterController instance;
    public static CharacterController Instance { get { return instance; } }
    public GameObject pistolShot;
    public PlayerMotionState motionState;
    public LayerMask MonsterLayer;
    public float ViewAngle = 45;
    public float ViewDisLimit = 4;
    public float moveSpeed = 10; 
    public float rotSpeed = 15;
    public float angleOffset;
    private Rigidbody rb;
    private Animator animator;
    private float aniVSpeed, aniHSpeed;
    private float testAngle = 0;
    float targetGunMaskWeight = 0;

    [SerializeField]
    private Collider[] monstersOverSeeing;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null) {
            instance = this;
        }

        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float hSpeed = Input.GetAxisRaw("Horizontal");
        float vSpeed = Input.GetAxisRaw("Vertical");
        Vector3 moveDir = new Vector3(hSpeed, 0, vSpeed).normalized;
        float inputSpeed = Mathf.Clamp01(new Vector3(hSpeed, 0, vSpeed).magnitude);
        
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            if(motionState != PlayerMotionState.Crouch)
            {
                moveSpeed *= 0.5f;
                animator.speed *= 0.5f;
                motionState = PlayerMotionState.Crouch;
            }
            else
            {
                moveSpeed *= 2;
                animator.speed *= 2;
                motionState = PlayerMotionState.Walk;
            }
        }

        if(hSpeed != 0 || vSpeed != 0)
            rb.velocity = moveDir * inputSpeed * moveSpeed;
        else
            rb.velocity = Vector3.zero;

        Vector3 mousePos = Input.mousePosition;
        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;
        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, (angle - 90) * -1, 0));
        //Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));
        //transform.LookAt(mousePos + Vector3.up * transform.position.y);

        aniVSpeed = Mathf.Lerp(aniVSpeed, vSpeed, 10 * Time.deltaTime);
        aniHSpeed = Mathf.Lerp(aniHSpeed, hSpeed, 10 * Time.deltaTime);
        animator.SetFloat("hSpeed", aniHSpeed);
        animator.SetFloat("vSpeed", aniVSpeed);
        DetectMonstorCycle();
    }
    private void Update()
    {

        if (Input.GetMouseButton(1))
        {
            targetGunMaskWeight = Mathf.Lerp(targetGunMaskWeight, 1, Time.deltaTime * 10);
            if (Input.GetMouseButtonDown(0)) 
            {
                animator.CrossFade("Shoot", 0.1f, 1);
                StartCoroutine(PistolShotFlashLight());
            }
        }
        else
        {
            targetGunMaskWeight = Mathf.Lerp(targetGunMaskWeight, 0, Time.deltaTime * 10);
        }

        animator.SetLayerWeight(1, targetGunMaskWeight);
        
    }
    IEnumerator PistolShotFlashLight() 
    {
        pistolShot.SetActive(true);

        yield return new WaitForSeconds(0.1f);
        pistolShot.SetActive(false);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        // Handles.color = Color.blue;
        // Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.05f);
        // Handles.DrawSolidArc(transform.position, transform.up, Quaternion.Euler(Vector3.up * -ViewAngle/2) * transform.forward, ViewAngle, ViewDisLimit);

        // Handles.color = Color.cyan;
        // Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.5f);
        // Handles.DrawWireArc(transform.position, transform.up, transform.forward, 360, ViewDisLimit);
    }
#endif
    private void DetectMonstorCycle()
    {
        var overlapColliders = Physics.OverlapSphere(transform.position, ViewDisLimit, MonsterLayer);
        for(int i=0 ; i<overlapColliders.Length ; i++)
        {
            var currentCollider = overlapColliders[i];
            
            float disToMonster = Vector3.Distance(transform.position, currentCollider.transform.position);
            Vector3 dirToTarget = (currentCollider.transform.position - transform.position).normalized;
            float angleToAngle = Vector3.Angle(transform.forward, dirToTarget);
            
            
            // enemy enter circle
            if(disToMonster <= ViewDisLimit )
            {
                SimpleAI currentMonsterAI = currentCollider.GetComponent<SimpleAI>();
                currentMonsterAI.SetMonsterVisibleSate(MonsterVisibleState.OnlyCanListen);
                if (angleToAngle <= (ViewAngle/2 + 10) )
                {
                    RaycastHit hitDetectEnemy;
                    Debug.DrawLine(transform.position + Vector3.up * 0.25f, transform.position + Vector3.up * 0.25f + dirToTarget * 10, Color.red);
                    Physics.Raycast(transform.position + Vector3.up * 0.25f, dirToTarget, out hitDetectEnemy, disToMonster);
                    if(currentCollider.gameObject == hitDetectEnemy.collider.gameObject)
                    {
                        currentMonsterAI.SetMonsterVisibleSate(MonsterVisibleState.Visible);
                    }
                }
            }
            
        }
        for(int i=0 ; i<monstersOverSeeing.Length ; i++)
        {
            if(!overlapColliders.Contains(monstersOverSeeing[i]))
            {
                SimpleAI currentMonster = monstersOverSeeing[i].GetComponent<SimpleAI>();
                currentMonster.SetMonsterVisibleSate(MonsterVisibleState.UnVisible);
            }
        }
        monstersOverSeeing = overlapColliders;
    }
}
