﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CockTailExplose : MonoBehaviour
{
    public bool launching = false;
    public float radius = 5.0F;
    public float power = 10.0F;
    public GameObject Spark;
    public Rigidbody[] fireBalls;
    private Transform oriTrans;
    private Vector3 oriRotation;
    private void Start()
    {
        oriRotation = transform.rotation.eulerAngles;
        oriTrans = transform.parent;
        Spark.SetActive(false);
        foreach(var member in fireBalls)
        {
           member.gameObject.SetActive(false); 
        }   
    }

    public void Initial()
    {
        transform.parent = oriTrans;
        transform.localPosition = Vector3.zero;
        Spark.SetActive(false);
        foreach(var member in fireBalls)
        {
            member.gameObject.SetActive(false); 
            member.useGravity = false;
            member.isKinematic = true;
        }
    }
    public void BurstFire()
    {
        transform.parent = null;
        transform.rotation = Quaternion.identity;
        StartCoroutine(WaitOneFrameToAddForce());
    }
    IEnumerator WaitOneFrameToAddForce()
    {
        Spark.SetActive(true);
        foreach(var member in fireBalls)
        {
            member.gameObject.SetActive(true); 
            Vector3 randOffset = Random.insideUnitSphere * radius/2.25f;
            member.transform.position = new Vector3(transform.position.x + randOffset.x, transform.position.y, transform.position.z + randOffset.z);
            member.useGravity = true;
            member.isKinematic = false;
        }
        yield return null;
        foreach(var member in fireBalls)
        {
            member.AddExplosionForce(Random.Range(150, power), transform.position, radius, 0.35f);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);   
    }
}
