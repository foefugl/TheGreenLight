﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataBase : CreatureDataBase, IEnumControl<MotionSpeedStatus>
{
    public static PlayerDataBase Instance { get {return instance; } }
    private static PlayerDataBase instance;
    public ItemObject InUsingItem { get; private set; }
    [SerializeField]
    private ValueBarSetting armor = new ValueBarSetting(0, 0, 100, 0, 0);
    [SerializeField]
    private ValueBarSetting san = new ValueBarSetting(100, 0, 100, 1/60, 0);
    [SerializeField]
    private ValueBarSetting energy = new ValueBarSetting(100, 0, 100, 5/60, 0);
    [SerializeField]
    private ValueBarSetting stamina = new ValueBarSetting(100, 0, 100, 0.25f, 0);
    [SerializeField]
    private ValueBarSetting hungry = new ValueBarSetting(100, 0, 100, 0, 2.5f/60);

    [SerializeField]
    private MotionSpeedStatus motionStatus = MotionSpeedStatus.Walk;
    public MotionSpeedStatus MotionStatus
    {
        get
        {
            return motionStatus;
        }
    }
    public ValueBarSetting Stamina { get { return stamina; } }
    private void Awake()
    {
        if(instance == null)
            instance = this;    
    }
    public override void FrameUpdate()
    {
        base.FrameUpdate();
        Stamina.FrameUpate();
    }
    public void CostEnergy(float _cost)
    {
        energy.Amount -= _cost;
    }
    public void RemoveEnum(MotionSpeedStatus _target)
    {
        if(HasEnum(_target))
            motionStatus &= ~_target;
        else
            Debug.LogWarning("Don't have this status : " + _target);
    }
    public void AddEnum(MotionSpeedStatus _target)
    {
        if(!HasEnum(_target))
            motionStatus |= _target;
        else
            Debug.LogWarning("Already have this type of status : " + _target);
    }
    public bool HasEnum(MotionSpeedStatus _target)
    {
        if(_target == MotionSpeedStatus.None)
            return false;
        else
            return  (motionStatus & _target) != 0;
    }
    public void ToggleEnum(MotionSpeedStatus _target)
    {
        motionStatus ^= _target;
    }
    public void OnlyFlagEnum(MotionSpeedStatus _target)
    {
        motionStatus &= 0;
        motionStatus |= _target;
    }
    new public void ClearEnum()
    {
        motionStatus &= 0;
    }
    public void SetUseItem(ItemObject _toUse)
    {
        if(_toUse == null){
            Debug.Log("Can't set null inside");
        }
        InUsingItem = _toUse;
    }
    public void EmptyUsingItem()
    {
        InUsingItem = null;
    }
}
