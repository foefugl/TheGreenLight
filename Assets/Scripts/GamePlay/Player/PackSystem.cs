﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackSystem : MonoBehaviour
{
    public virtual void RemoveFromBag(ItemObject target, bool throwOutFlag)
    {

    }
    public virtual void AddToBag(ItemObject target)
    {

    }
}
