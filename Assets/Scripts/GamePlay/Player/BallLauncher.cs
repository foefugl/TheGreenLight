﻿using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using System.Collections;
using DG.Tweening;

public class BallLauncher : MonoBehaviour 
{
	public BagPackSystem bagPackSystem;
	public float throwDis = 2.5f;
	public Transform currentThowPoint;
	public LayerMask floorLayer;
	public LineRenderer lineRend;
	public Rigidbody holdObjRB;
	public Transform target;

	public float h = 25;
	public float gravity = -18;
	public int resolution = 30;

	public bool debugPath;
	public UnityEvent armingEvents;
	public UnityEvent returnEvents;
	private Vector3 relatedCenter, relatedDir, ballInitialPos, ballInitialRot;
	private GameObject HoldingObj;
	private ItemObject HoldingItemData;
	private BagGrid usingGrid;

	void Start() 
	{
		lineRend = GetComponent<LineRenderer>();
		bagPackSystem = GameObject.FindObjectOfType<BagPackSystem>();
		lineRend.positionCount = resolution;
	}
	public void GrabSwitchItem(ItemObject _itemObject, BagGrid _fillGrid)
	{
		if(HoldingObj != null)
			HoldingObj.SetActive(false);

		usingGrid = _fillGrid;
		HoldingItemData = _itemObject;
		HoldingObj = _itemObject.transform.GetChild(0).transform.gameObject;
		HoldingObj.transform.parent = currentThowPoint;
		HoldingObj.transform.transform.localPosition = Vector3.zero;
		HoldingObj.transform.localRotation = Quaternion.identity;
		HoldingObj.SetActive(true);
		holdObjRB = HoldingObj.GetComponent<Rigidbody>();
	}
	public void GrabItem(ItemObject _itemObject, BagGrid _fillGrid)
	{
		usingGrid = _fillGrid;
		HoldingItemData = _itemObject;
		HoldingObj = _itemObject.transform.GetChild(0).transform.gameObject;
		HoldingObj.transform.parent = currentThowPoint;
		HoldingObj.transform.transform.localPosition = Vector3.zero;
		HoldingObj.transform.localRotation = Quaternion.identity;
		HoldingObj.SetActive(true);
		holdObjRB = HoldingObj.GetComponent<Rigidbody>();
	}
	void Update() 
	{
		if(Input.GetMouseButtonUp(1) && HoldingObj != null)
		{
			armingEvents.Invoke();
		}

		if(!Input.GetMouseButton(1) || HoldingObj == null)
		{
			lineRend.enabled = false;
			return;
		}
		if(Input.GetMouseButtonDown(1))
		{
			armingEvents.Invoke();
			lineRend.enabled = true;
		}
		
			
		if (Input.GetMouseButtonDown(0)) 
		{
			bagPackSystem.UsedUpItem(HoldingItemData);
			usingGrid.filled = false;
			HoldingItemData = null;
			HoldingObj = null;
			Launch ();
		}

		if (debugPath) {
			DrawPath ();
		}
		
	}
	void LateUpdate()
	{
		UpdateTargetPos();	
	}
	private void UpdateTargetPos()
	{
		RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit, 100, floorLayer)) 
		{
			relatedCenter = new Vector3(transform.position.x, hit.point.y, transform.position.z);
			relatedDir = (hit.point - relatedCenter).normalized;

			if(Vector3.Distance(hit.point, relatedCenter)<= throwDis)
			{
            	target.position = hit.point;
			}
			else
			{
				Vector3 relatedPoint = relatedCenter + relatedDir * throwDis;
				target.position = relatedPoint;
			}
        }
	}
	public LaunchData LaunchDataReturn(Rigidbody stone, Vector3 targetPos)
	{
		return CalculateLaunchData(targetPos, stone.transform);
	}
	public void Launch(Rigidbody stone, Vector3 targetPos)
	{
		holdObjRB = stone;
		target.position = targetPos;
		Launch();
	}

	void Launch() 
	{
		Physics.gravity = Vector3.up * gravity;
		holdObjRB.useGravity = true;
		holdObjRB.isKinematic = false;
		var data = CalculateLaunchData ();
		holdObjRB.velocity = data.initialVelocity;
		// holdObjRB.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
		// float dir = (relatedDir.x > 0)? -1 : 1 ;
		// holdObjRB.angularVelocity = Vector3.forward * dir * 360 * 2.25f;
	}

	LaunchData CalculateLaunchData(Vector3 _target, Transform _ball) 
	{
		float displacementY = _target.y - _ball.position.y;
		Vector3 displacementXZ = new Vector3 (_target.x - _ball.position.x, 0, _target.z - _ball.position.z);
		float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
		Vector3 velocityXZ = displacementXZ / time;

		return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	}
	LaunchData CalculateLaunchData() 
	{
		float displacementY = target.position.y - holdObjRB.position.y;
		Vector3 displacementXZ = new Vector3 (target.position.x - holdObjRB.position.x, 0, target.position.z - holdObjRB.position.z);
		float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
		Vector3 velocityXZ = displacementXZ / time;

		return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	}

	void DrawPath() {
		LaunchData launchData = CalculateLaunchData ();
		Vector3 previousDrawPoint = holdObjRB.position;
		Vector3[] linePoints = new Vector3[resolution];
		linePoints[0] = previousDrawPoint;

		for (int i = 1; i <= resolution; i++) {
			float simulationTime = i / (float)resolution * launchData.timeToTarget;
			Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
			Vector3 drawPoint = holdObjRB.position + displacement;
			linePoints[i-1] = drawPoint;
			Debug.DrawLine (previousDrawPoint, drawPoint, Color.green);
			previousDrawPoint = drawPoint;
		}
		lineRend.SetPositions(linePoints);
	}
// #if UNITY_EDITOR
// 	private void OnDrawGizmos() 
// 	{
// 		Handles.color = Color.yellow;
// 		Handles.DrawWireArc(transform.position, Vector3.up, transform.forward, 360, throwDis);
// 	}
// #endif

	public struct LaunchData 
	{
		public readonly Vector3 initialVelocity;
		public readonly float timeToTarget;

		public LaunchData (Vector3 initialVelocity, float timeToTarget)
		{
			this.initialVelocity = initialVelocity;
			this.timeToTarget = timeToTarget;
		}
		
	}
}
	