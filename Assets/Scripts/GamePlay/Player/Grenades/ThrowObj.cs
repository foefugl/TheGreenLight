﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class ThrowObj : MonoBehaviour
{
    public bool hasBeenLaunch;
    public float lifeTime = 5.0f;
    public UnityEvent hitEvents;
    protected Transform handParent;
    [HideInInspector]
    public Transform objPoolParent;
    protected Transform vfxBomb;
    protected Vector3 oriRotation, oriLocalPos;
    private Collider selfCollider, currentPitcher;
    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();    
    }
    public virtual void StartStep(Collider pitcher)
    {
        hasBeenLaunch = false;
        vfxBomb = transform.GetChild(0);
        selfCollider = GetComponent<Collider>();
        currentPitcher = pitcher;
        Physics.IgnoreCollision(selfCollider, currentPitcher);
        oriRotation = transform.rotation.eulerAngles;
        handParent = transform.parent;
        oriLocalPos = transform.localPosition;
    }
    public virtual void Initial(Collider pitcher)
    {
        hasBeenLaunch = false;
        currentPitcher = pitcher;
        Physics.IgnoreCollision(selfCollider, currentPitcher);

        // 投擲物顯示層位置重新設定
        transform.parent = handParent;
        transform.localPosition = oriLocalPos;
        transform.rotation = Quaternion.Euler(oriRotation);
        // VFX反應物位置重新設定
        if(vfxBomb != null)
        {
            vfxBomb.parent = transform;
            vfxBomb.localRotation = Quaternion.identity;
            vfxBomb.localPosition = Vector3.zero;
        }
    }
    protected virtual void TurnBackParent()
    {
        rb = GetComponent<Rigidbody>();   
        hasBeenLaunch = false;
        Physics.IgnoreCollision(selfCollider, currentPitcher, false);
        currentPitcher = null;

        // 投擲物顯示層位置重新設定
        transform.parent = objPoolParent;
        transform.localPosition = oriLocalPos;
        transform.rotation = Quaternion.Euler(oriRotation);
        // VFX反應物位置重新設定
        if(vfxBomb != null)
        {
            vfxBomb.parent = transform;
            vfxBomb.localRotation = Quaternion.identity;
            vfxBomb.localPosition = Vector3.zero;
        }
        rb.useGravity = true;
        rb.isKinematic = true;
        gameObject.SetActive(false);
        ItemObjectPool.Instance.Return(objPoolParent.GetComponent<ItemObject>());
    }
    public virtual void ToggleLaunchState()
    {
        Debug.Log("Launch Obj");
    }
    protected virtual void HitEvent()
    {
        hitEvents.Invoke();
        if(vfxBomb != null)
        {
            vfxBomb.parent = null;
            vfxBomb.up = Vector3.up;
        }
        StartCoroutine(LifeTimeCounter());
    }
    private void OnCollisionEnter(Collision other) 
    {
        if(hasBeenLaunch)
            return;

        hasBeenLaunch = true;
        HitEvent();
    }
    protected IEnumerator LifeTimeCounter()
    {
        yield return new WaitForSeconds(lifeTime);
        TurnBackParent();
    }
}
