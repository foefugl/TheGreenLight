﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Molotov : ThrowObj
{
    public float exploseRadius = 5.0f;
    public float explosePow = 10.0f;
    public GameObject launchingVfx;
    public GameObject spark;
    public AttackFire[] fireBalls;
    private void Awake() 
    {
        ParticleSystem[] allChildsPS = GetComponentsInChildren<ParticleSystem>();
        foreach(var member in allChildsPS)
        {
            var mainModul = member.main;
            mainModul.duration = lifeTime;

            if(member.transform.name == "Light")
                mainModul.duration *= 0.9f;
        }
    }
    private void OnEnable() 
    {
        if(!gameObject.activeSelf)
            return;
        
        base.StartStep(transform.root.GetComponent<Collider>());
        spark.SetActive(false);
        foreach(var member in fireBalls)
        {
           member.gameObject.SetActive(false); 
        }   
    }
    public override void Initial(Collider pitcher)
    {
        if(!gameObject.activeSelf)
            return;

        base.Initial(pitcher);

        spark.SetActive(false);
        foreach(var member in fireBalls)
        {
            member.gameObject.SetActive(false); 
            member.rb.useGravity = false;
            member.rb.isKinematic = true;
        }
    }
    protected override void TurnBackParent()
    {
        base.TurnBackParent();
        spark.SetActive(false);
        foreach(var member in fireBalls)
        {
            member.gameObject.SetActive(false); 
            member.rb.useGravity = false;
            member.rb.isKinematic = true;
        }
    }
    public override void ToggleLaunchState()
    {
        launchingVfx.SetActive(!launchingVfx.activeSelf);
    }
    protected override void HitEvent()
    {
        base.HitEvent();
        launchingVfx.SetActive(false);
        StartCoroutine(WaitOneFrameToAddForce());
        StartCoroutine(LifeTimeCounter());
    }
    IEnumerator WaitOneFrameToAddForce()
    {
        spark.SetActive(true);
        foreach(var member in fireBalls)
        {
            member.gameObject.SetActive(true); 
            Vector3 randOffset = Random.insideUnitSphere * exploseRadius/2.25f;
            member.transform.position = new Vector3(transform.position.x + randOffset.x, transform.position.y, transform.position.z + randOffset.z);
            member.rb.useGravity = true;
            member.rb.isKinematic = false;
        }
        yield return null;
        foreach(var member in fireBalls)
        {
            member.rb.AddExplosionForce(Random.Range(150, explosePow), transform.position, exploseRadius, 0.35f);
        }
    }
}
