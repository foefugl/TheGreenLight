﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackFire : MonoBehaviour
{
    public bool landed = false;
    public Rigidbody rb;
    public DoTData doTdata;
    private Collider col;
    private Vector3 anchorPoint;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();   
    }
    public void Initial()
    {
        landed = false;
    }
    private void SetAnchor(Vector3 _point)
    {
        rb.useGravity = false;
        rb.isKinematic = true;
        anchorPoint = _point;
    }
    void OnTriggerEnter(Collider other)
    {
        landed = true;
        SetAnchor(transform.position);
        if(other.gameObject.layer != LayerMask.NameToLayer("Creature") && other.gameObject.layer != LayerMask.NameToLayer("Player"))
            return;

        if(!landed)
        {
            transform.parent = other.transform;
        }
        else
        {
            var creatureData = other.GetComponent<CreatureDataBase>();
            if(creatureData == null)
                creatureData = other.GetComponentInChildren<CreatureDataBase>();
            if(creatureData == null)
                creatureData = other.GetComponentInParent<CreatureDataBase>();

            var damager = other.GetComponent<DamageTaker>();
            if(damager == null)
                damager = other.GetComponentInChildren<DamageTaker>();
            if(damager == null)
                damager = other.GetComponentInParent<DamageTaker>();

            if(creatureData.HasEnum(DeBuff.Burning))
                return;
            
            creatureData.AddEnum(DeBuff.Burning);
            damager.BurningDamage(doTdata);
        }
    }
}
