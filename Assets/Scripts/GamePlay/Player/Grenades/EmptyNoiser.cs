﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyNoiser : ThrowObj
{
    private void OnEnable() 
    {
        base.StartStep(transform.root.GetComponent<Collider>()); 
        vfxBomb.gameObject.SetActive(false);
    }
    protected override void TurnBackParent()
    {
        base.TurnBackParent();
        vfxBomb.gameObject.SetActive(false);
    }
    public override void Initial(Collider pitcher)
    {
        base.Initial(pitcher);
        vfxBomb.gameObject.SetActive(false);
    }
    protected override void HitEvent()
    {
        base.HitEvent();
        CauseSound();
    }
    private void CauseSound()
    {
        vfxBomb.transform.position -= Camera.main.transform.forward * 5;
        vfxBomb.gameObject.SetActive(true);
    }
}
