﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEditor;

public class BagPackSystem : PackSystem
{
    public bool opening;
    public float collectRadius;
    public LayerMask itemLayer;
    public RectTransform[] toolsBar;
    private int selectBarIndex;
    public GameObject BackPack;
    public Transform emptyItemUIStorage;
    public GameObject itemUIPrefab;
    public BallLauncher ThrowWeaponApplier;
    public List<BagGrid> itemGrids;
    private ToolBagGrid selectingToolGrid;
    private ItemObject inUsingItem;
    private List<PackItemData> itemUIs;
    private Dictionary<int, List<ItemObject>> entityContentDic;
    private Dictionary<int, PackItemData> bagGridUIDataDic;
    private void Awake() 
    {
        entityContentDic = new Dictionary<int, List<ItemObject>>();
        bagGridUIDataDic = new Dictionary<int, PackItemData>();
        itemUIs = new List<PackItemData>();
        for(int i=0 ; i<itemGrids.Count ; i++)
        {
            PackItemData uiData = Instantiate(itemUIPrefab, emptyItemUIStorage.position, emptyItemUIStorage.rotation).GetComponent<PackItemData>();
            uiData.transform.SetParent(emptyItemUIStorage);
            uiData.transform.localPosition = Vector3.zero;
            uiData.gameObject.SetActive(false);
            itemUIs.Add(uiData);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if(!opening)
            BackPack.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Collecting
        if(Input.GetKeyDown(KeyCode.Space))
            CollectObject();

        // Bag
        if(Input.GetKeyDown(KeyCode.B)){
            opening = !opening;
            BackPack.SetActive(opening);
        }

        // ShortCutKeyDown
        if(Input.GetKeyDown(KeyCode.Alpha1))
            ShortCutItemSelect(0);
        else if(Input.GetKeyDown(KeyCode.Alpha2))
            ShortCutItemSelect(1);
        else if(Input.GetKeyDown(KeyCode.Alpha3))
            ShortCutItemSelect(2);
        else if(Input.GetKeyDown(KeyCode.Alpha4))
            ShortCutItemSelect(3);

        if(selectingToolGrid != null && PlayerDataBase.Instance.InUsingItem == null)
        {
            if(selectingToolGrid.selectImage.gameObject.activeSelf)
                selectingToolGrid.SetSelectFrame(false);
        }
    }
    private void CollectObject()
    {
        Collider[] objectsOverlap = Physics.OverlapSphere(transform.position, collectRadius, itemLayer);
        for(int i=0 ; i<objectsOverlap.Length ; i++)
        {
            var itemObject = objectsOverlap[i].GetComponent<ItemObject>();
            itemObject.BeCollected(transform, Vector3.Distance(itemObject.transform.position, transform.position)/collectRadius * 0.325f);
            AddToBag(itemObject);
        }
    }
    public override void AddToBag(ItemObject itemObject)
    {
        if(IsEntityHaveIt(itemObject.itemData.ID))
        {
            entityContentDic[itemObject.itemData.ID].Add(itemObject);
        }
        else
        {
            List<ItemObject> newList = new List<ItemObject>();
            newList.Add(itemObject);
            entityContentDic.Add(itemObject.itemData.ID, newList);
        }
        AddBagInfo(itemObject);
    }
    public void UsedUpItem(ItemObject _itemObj)
    {
        if(IsEntityHaveIt(_itemObj.itemData.ID))
        {
            List<ItemObject> list = entityContentDic[_itemObj.itemData.ID];
            var topObj = list.Last();
            ItemObjectPool.Instance.Return(topObj);

            if(list.Count == 1)
                entityContentDic.Remove(_itemObj.itemData.ID);

            list.RemoveAt(list.Count - 1);
        }
        RemoveBagInfo(_itemObj);
    }
    public override void RemoveFromBag(ItemObject _itemObj, bool throwOutFlag)
    {
        if(IsEntityHaveIt(_itemObj.itemData.ID))
        {
            List<ItemObject> list = entityContentDic[_itemObj.itemData.ID];
            var topObj = list.Last();

            if(topObj == PlayerDataBase.Instance.InUsingItem)
                PlayerDataBase.Instance.EmptyUsingItem();
                
            if(throwOutFlag)
                topObj.BeThrowed();

            if(list.Count == 1)
                entityContentDic.Remove(_itemObj.itemData.ID);

            list.RemoveAt(list.Count - 1);
        }
        RemoveBagInfo(_itemObj);
    }
    private void RemoveBagInfo(ItemObject _itemObj)
    {
        PackItemData packItemData = bagGridUIDataDic[_itemObj.itemData.ID];
        if(entityContentDic.ContainsKey(_itemObj.itemData.ID))
        {
            packItemData.UpdateGridContent(_itemObj, entityContentDic[_itemObj.itemData.ID].Count);
        }
        else
        {
            packItemData.ClearData();
            packItemData.gameObject.SetActive(false);
            packItemData.transform.parent = emptyItemUIStorage;
            packItemData.transform.localPosition = Vector3.zero;
            bagGridUIDataDic.Remove(_itemObj.itemData.ID);
        }
    }
    private void AddBagInfo(ItemObject _itemObj)
    {
        if(bagGridUIDataDic.ContainsKey(_itemObj.itemData.ID))
        {
            bagGridUIDataDic[_itemObj.itemData.ID].UpdateGridContent(_itemObj, entityContentDic[_itemObj.itemData.ID].Count);
        }
        else
        {
            for(int i=0 ; i<itemGrids.Count ; i++)
            {
                if(!itemGrids[i].filled)
                {
                    itemGrids[i].filled = true;
                    var emptyUIBeSelected = SelectEmptyItemUI();
                    emptyUIBeSelected.beLongPackSystem = this;
                    emptyUIBeSelected.gameObject.SetActive(true);
                    emptyUIBeSelected.itemImage.transform.parent = itemGrids[i].transform;
                    emptyUIBeSelected.itemImage.rectTransform.anchoredPosition = Vector3.zero;
                    emptyUIBeSelected.UpdateGridContent(_itemObj, entityContentDic[_itemObj.itemData.ID].Count);
                    bagGridUIDataDic.Add(_itemObj.itemData.ID, emptyUIBeSelected);
                    break;
                }
            }
        }
    }
    private PackItemData SelectEmptyItemUI()
    {
        PackItemData answer = null;
        for(int i=0 ; i<itemUIs.Count ; i++)
        {
            if(itemUIs[i].gameObject.activeSelf == false)
            {
                answer = itemUIs[i];
                break;
            }
        }
        return answer;
    }
    private bool IsEntityHaveIt(int id)
    {
        return entityContentDic.ContainsKey(id);
    }
    private void ShortCutItemSelect(int index)
    {
        if(selectingToolGrid != null)
            selectingToolGrid.SetSelectFrame(false);

        selectBarIndex = index;
        selectingToolGrid = toolsBar[index].GetComponent<ToolBagGrid>();
        selectingToolGrid.SetSelectFrame(true);

        PackItemData itemData = toolsBar[index].GetComponentInChildren<PackItemData>();

        if(itemData != null)
        {
            List<ItemObject> _list = entityContentDic[itemData.CurrentItemList[0].itemData.ID];
            ItemObject objToUse = _list.Last();
            PlayerDataBase.Instance.SetUseItem(objToUse);
            BagGrid bagGrid = bagGridUIDataDic[objToUse.itemData.ID].transform.parent.GetComponent<BagGrid>();
            
            if(itemData != null && !objToUse.itemData.ObjectName.Contains("Pipe"))
                ThrowWeaponApplier.GrabSwitchItem(objToUse, bagGrid);
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Handles.DrawWireArc(transform.position, Vector3.up, transform.forward, 360, collectRadius);
    }
#endif
}
