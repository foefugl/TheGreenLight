﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIData : CreatureUIData
{
    public Image energyBar;
    private PlayerDataBase playerData;
    protected override void Initial()
    {
        base.Initial();
        playerData = creatureData as PlayerDataBase;
    }
    protected override void FrameUpdate()
    {
        base.FrameUpdate();
        energyBar.fillAmount = playerData.Stamina.Amount / playerData.Stamina.Max; 
    }
}
