﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ItemObject : MonoBehaviour
{
    public Item itemData;
    private Rigidbody rb;
    private SpriteRenderer spriteRend;
    private Material mat;
    private Vector3 oriScale;

    [ExecuteInEditMode]
    private void OnValidate() 
    {
        if(itemData == null)
            return;
            
        spriteRend = GetComponent<SpriteRenderer>();
        oriScale = Vector3.one;
        spriteRend.sprite = itemData.sprite;
        gameObject.name = '#' + itemData.name;
    }

    private void Start()
    {
        gameObject.SetActive(false);
        oriScale = Vector3.one;
        Initial();
        
        if(itemData.HandheldObjPrefab != null)
            SpawnHandheld();
    }
    private void SpawnHandheld()
    {
        GameObject handheld = Instantiate(itemData.HandheldObjPrefab, transform.position, Quaternion.identity);
        handheld.GetComponent<ThrowObj>().objPoolParent = transform;
        handheld.transform.parent = transform;
        handheld.SetActive(false);
    }
    public void BeTaken()
    {
        rb.useGravity = true;
        rb.isKinematic = false;
        rb.velocity = Vector3.zero;
        StartCoroutine(LifeTimeTimer());
        ThrowMotion();
    }
    public void BeReturn()
    {
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        spriteRend.material.SetFloat("_Opacity", 1);
        StopAllCoroutines();
    }
    public ItemObject Initial(Item _itemData)
    {
        itemData = _itemData;
        oriScale = transform.localScale;

        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        rb.isKinematic = true;

        spriteRend = GetComponent<SpriteRenderer>();
        spriteRend.sprite = itemData.sprite;

        gameObject.name = '#' + itemData.name;

        mat = new Material(spriteRend.material.shader);
        mat.SetTexture("_MainTex", spriteRend.material.GetTexture("_MainTex"));
        mat.SetTexture("_GradientTex", spriteRend.material.GetTexture("_GradientTex"));
        mat.SetFloat("_OutlineValue", spriteRend.material.GetFloat("_OutlineValue"));
        mat.SetColor("_OutlineColor", spriteRend.material.GetColor("_OutlineColor"));
        mat.SetFloat("_Opacity", 1);
        
        spriteRend.material = mat;
        return this;
    }
    public ItemObject Initial()
    {
        oriScale = transform.localScale;

        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        rb.isKinematic = true;

        spriteRend = GetComponent<SpriteRenderer>();
        spriteRend.sprite = itemData.sprite;

        gameObject.name = '#' + itemData.name;

        mat = new Material(spriteRend.material.shader);
        mat.SetTexture("_MainTex", spriteRend.material.GetTexture("_MainTex"));
        mat.SetTexture("_GradientTex", spriteRend.material.GetTexture("_GradientTex"));
        mat.SetFloat("_OutlineValue", spriteRend.material.GetFloat("_OutlineValue"));
        mat.SetColor("_OutlineColor", spriteRend.material.GetColor("_OutlineColor"));
        mat.SetFloat("_Opacity", 1);

        spriteRend.material = mat;
        return this;
    }
    public void BeThrowed()
    {
        transform.parent = null;
        rb.isKinematic = false;
        rb.useGravity = true;
        spriteRend.material.SetFloat("_Opacity", 1);
        gameObject.SetActive(true);
        StartCoroutine(LifeTimeTimer());
        ThrowMotion();
    }
    public void ThrowMotion()
    {
        transform.localScale = oriScale; 
        float throwF = 1f;
        float horizontalAngle = Mathf.Deg2Rad * Random.Range(0, 360);
        rb.velocity = new Vector3(Mathf.Cos(horizontalAngle) * throwF, throwF * 3, Mathf.Sin(horizontalAngle) * throwF);
    }
    public void BeCollected(Transform center, float duration)
    {
        StopAllCoroutines();
        rb.isKinematic = true;
        rb.useGravity = false;
        float alphaValue = 1;
        DOTween.To(() => alphaValue, x => alphaValue = x, 0, duration)
        .OnUpdate(() => 
        {
            spriteRend.material.SetFloat("_Opacity", alphaValue);
        });
        transform.DOMove(center.position,  duration)
        .OnComplete(() =>
        {
            gameObject.SetActive(false);
            transform.parent = center;
            transform.localPosition = Vector3.zero;
        });
        transform.DOScale(Vector3.one * 0.2f, duration)
        .OnComplete(() => 
        {
            transform.localScale = Vector3.one;
        });
    }
    public void JustBeCollected(Transform center)
    {
        StopAllCoroutines();
        rb.isKinematic = true;
        rb.useGravity = false;
        gameObject.SetActive(false);
        transform.parent = center;
        transform.localPosition = Vector3.zero;
    }
    IEnumerator LifeTimeTimer()
    {
        float waringT = 3;
        yield return new WaitForSeconds(itemData.lifeTime - waringT);
        // Start Warning
        StartCoroutine(FlashWarningToDisappear(waringT));
        yield return new WaitForSeconds(waringT);
        // Return Pool
        ItemObjectPool.Instance.Return(this);
    }
    IEnumerator FlashWarningToDisappear(float limit)
    {
        float counter = 0;
        while(counter <= limit)
        {
            counter += Time.deltaTime;
            spriteRend.material.SetFloat("_Opacity", Mathf.Clamp01((1-counter)/limit));
            yield return null; 
        }
        spriteRend.material.SetFloat("_Opacity", 1);
    }
}
