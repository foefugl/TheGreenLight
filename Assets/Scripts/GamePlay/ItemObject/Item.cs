﻿using UnityEngine;

[CreateAssetMenu(menuName = "My Asset/Item")]
public class Item : PoolObjScriptable
{
    public int LimitPerGrid;
    public Sprite sprite;
    public GameObject HandheldObjPrefab;
}
