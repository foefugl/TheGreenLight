﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class ItemTestSpawnPoint : MonoBehaviour
{
    public KeyCode spawnKey, returnKey;
    public Item spawnItemType;
    private List<ItemObject> TakeOutList;
    private void Start() 
    {
        TakeOutList = new List<ItemObject>();    
    }
    private void Update() 
    {
        if(Input.GetKeyDown(spawnKey))
        {
            ItemObject item = ItemObjectPool.Instance.Take(spawnItemType, transform.position, transform.rotation.eulerAngles);
            if(item != null)
                TakeOutList.Add(item);
        }
        else if(Input.GetKeyDown(returnKey))
        {
            if(TakeOutList.Count == 0){
                Debug.Log(spawnItemType.ObjectName + " all has been return.");
                return;
            }
            ItemObject returnTarget = TakeOutList.Last();
            ItemObjectPool.Instance.Return(returnTarget);    
            TakeOutList.RemoveAt(TakeOutList.Count - 1);
        }
    }
}
