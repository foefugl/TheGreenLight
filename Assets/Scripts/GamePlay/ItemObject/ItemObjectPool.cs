﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ItemObjectPool : MonoBehaviour
{
    private static ItemObjectPool instance;
    public static ItemObjectPool Instance { get { return instance; } }
    public GameObject itemObjPrefab;
    public List<ItemPoolData> poolObjectDatas;
    private Dictionary<int, ItemPoolData> searchDic;
    private void Awake()
    {
        if(instance == null)
            instance = this;

        searchDic = new Dictionary<int, ItemPoolData>();
        for(int i=0 ; i<poolObjectDatas.Count ; i++)
        {
            poolObjectDatas[i].Clip  = new List<ItemObject>();
            poolObjectDatas[i].Cabinet = new GameObject().transform;
            poolObjectDatas[i].Cabinet.parent = transform;
            poolObjectDatas[i].Cabinet.localPosition = Vector3.zero;
            poolObjectDatas[i].Cabinet.name = "[" + poolObjectDatas[i].name + "]";
            for(int j=0 ; j<poolObjectDatas[i].amount ; j++)
            {
                GameObject spawned = Instantiate(itemObjPrefab, poolObjectDatas[i].Cabinet.position, Quaternion.identity);
                spawned.transform.parent = poolObjectDatas[i].Cabinet;
                spawned.transform.localPosition = Vector3.zero;
                ItemObject item = spawned.GetComponent<ItemObject>().Initial(poolObjectDatas[i].itemData);
                poolObjectDatas[i].Clip.Add(item);
            }
            
            searchDic.Add(poolObjectDatas[i].itemData.ID, poolObjectDatas[i]);
        }   
    }

    public ItemObject Take(Item itemData, Vector3 _pos, Vector3 _rot)
    {
        ItemObject answer = searchDic[itemData.ID].Take();
        if(answer == null){
            Debug.Log(itemData.ObjectName + "'s pool is empty.");
            return null;
        }

        answer.transform.parent = null;
        answer.transform.position = _pos;
        answer.transform.rotation = Quaternion.Euler(_rot);
        return answer;
    
    }
    public void Return(ItemObject target)
    {
        searchDic[target.itemData.ID].Return(target);
    }
}
[Serializable]
public class ItemPoolData : PoolObjectlData<ItemObject>
{
    public Item itemData;
    public override ItemObject Take()
    {
        ItemObject answer = base.Take();
        if(answer == null){
            Debug.Log(itemData.name + " take out by treasure.");
            return null;
        }
        
        answer.gameObject.SetActive(true);
        answer.BeTaken();
        return answer;
    }
    public override void Return(ItemObject target)
    {
        target.gameObject.SetActive(false);
        target.BeReturn();
        target.transform.parent = Cabinet;
        target.transform.localPosition = Vector3.zero;
        target.transform.rotation = Quaternion.identity;
    }
}
