﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PSObjectPool : MonoBehaviour
{
    private static PSObjectPool instance;
    public static PSObjectPool Instance { get { return instance; } }
    public List<PSPoolData> poolObjectDatas;
    private Dictionary<int, PSPoolData> searchDic;

    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
            instance = this;
        searchDic = new Dictionary<int, PSPoolData>();
        for(int i=0 ; i<poolObjectDatas.Count ; i++)
        {
            poolObjectDatas[i].Clip  = new List<VfxObject>();
            poolObjectDatas[i].Cabinet = new GameObject().transform;
            poolObjectDatas[i].Cabinet.parent = transform;
            poolObjectDatas[i].Cabinet.localPosition = Vector3.zero;
            poolObjectDatas[i].Cabinet.name = "[" + poolObjectDatas[i].name + "]";
            for(int j=0 ; j<poolObjectDatas[i].amount ; j++)
            {
                GameObject spawned = Instantiate(poolObjectDatas[i].vfxSetting.basePrefab, poolObjectDatas[i].Cabinet.position, Quaternion.identity);
                spawned.transform.parent = poolObjectDatas[i].Cabinet;
                spawned.transform.localPosition = Vector3.zero;
                VfxObject currentVfx = spawned.GetComponent<VfxObject>();
                poolObjectDatas[i].Clip.Add(currentVfx);
            }
            searchDic.Add(poolObjectDatas[i].vfxSetting.ID, poolObjectDatas[i]);
        }
    }
    public GameObject Take(int id, Vector3 _pos, Vector3 _rot, bool autoReturn)
    {
        GameObject answer = searchDic[id].Take().gameObject;
        if(answer == null){
            Debug.Log(searchDic[id].vfxSetting.name + "'s pool is empty.");
            return null;
        }

        answer.transform.parent = null;
        answer.transform.position = _pos;
        answer.transform.rotation = Quaternion.Euler(_rot);

        if(autoReturn)
        {
            var currentVfx = answer.GetComponent<VfxObject>();
            StartCoroutine(AutoReturn(answer, currentVfx.objectSetting.lifeTime));
        }
            
        return answer;
    }
    public void Return(GameObject target)
    {
        VfxObject currentVfx = target.GetComponent<VfxObject>();
        int id = currentVfx.objectSetting.ID;
        searchDic[id].Return(currentVfx);
    }
    IEnumerator AutoReturn(GameObject target, float dur)
    {
        yield return new WaitForSeconds(dur);
        Return(target);
    }
}
[System.Serializable]
public class PSPoolData : PoolObjectlData<VfxObject>
{
    public VfxScriptable vfxSetting;
    public override VfxObject Take()
    {
        VfxObject answer = base.Take();
        answer.gameObject.SetActive(true);
        return answer;
    }
    public override void Return(VfxObject target)
    {
        target.gameObject.SetActive(false);
        target.transform.parent = Cabinet;
        target.transform.localPosition = Vector3.zero;
        target.transform.rotation = Quaternion.identity;
    }
}
