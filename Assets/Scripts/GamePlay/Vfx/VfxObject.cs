﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxObject : PoolObjectEntity
{
    private ParticleSystem par;
    protected override void Initial()
    {
        base.Initial();
        par = GetComponent<ParticleSystem>();
    }
    protected override void BeTaken()
    {
        base.BeTaken();
        par.Play();
    }
    protected override void BeReturn()
    {
        LifeTimeEndAction = () =>
        {
            PSObjectPool.Instance.Return(gameObject);
        };
        par.Stop();
        base.BeReturn();
    }
}