﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class DamageTaker : MonoBehaviour
{
    public List<StatusFeedBack> statusVfxs;
    public UnityEvent BeShortAttack;
    private Dictionary<string, StatusFeedBack> statusVfxDic;
    private CreatureDataBase dataBase;
    private Renderer rend;
    private Color oriColor;
    private Tween colorTwn;
    
    private void Start() 
    {
        statusVfxDic = new Dictionary<string, StatusFeedBack>();
        for(int i=0 ; i<statusVfxs.Count ; i++)
        {
            statusVfxDic.Add(statusVfxs[i].name, statusVfxs[i]);
        }

        rend = GetComponent<Renderer>();
        if(rend == null)
            rend = GetComponentInChildren<Renderer>();
        
        oriColor = rend.material.GetColor("_Color");
        dataBase = GetComponent<CreatureDataBase>();    
    }
    public void ShortDamage(Color targetColor, float damagePoint)
    {
        if(gameObject.CompareTag("Player")){
            CinemachineShake.Instance.ShakeCamera(0.7f, 0.35f);
            PPManager.Instance.SetRangeBlur(0.0095f, 1, 0.4f);
        }
        dataBase.HP -= damagePoint;
        BeShortAttack.Invoke();
        statusVfxDic["PhysicalDamage"].vfx.Play();
        SetDamageColorOnce(targetColor, 0.6f);
    }
    public void BurningDamage(DoTData _data)
    {
        Action DamageCostAction = ()=>
        {
            dataBase.HP -= _data.PerUnitDamage;
        };
        StartCoroutine(DoActionPeriod(_data, DamageCostAction));    
    }
    public void DeadColorFeedback()
    {
        if(dataBase.HasEnum(DeBuff.Burning))
        {
            SetColorTo(statusVfxDic["Burning"].DeadColor, 0.5f);
        }
        else if(dataBase.HasEnum(DeBuff.Poisoning))
        {
            // SetDamageColorOnce(statusVfxDic["Poison"].feedbackColor, 0.5f);
        }
        else if(dataBase.HasEnum(DeBuff.ElectricShock))
        {

        }
        else if(dataBase.HasEnum(DeBuff.Weak))
        {

        }
        else if(dataBase.HasEnum(DeBuff.Fear))
        {

        }
        StartCoroutine(DisappearAfterDead(2.5f));
    }
    private void StopColorTwn()
    {
        if(colorTwn != null)
        {
            if(colorTwn.IsPlaying())
            {
                colorTwn.Kill();
            }
        }
    }
    private void SetColorTo(Color target, float _dur)
    {
        StopColorTwn();
        Color colorLerper = rend.material.GetColor("_Color");
        colorTwn = DOTween.To(() => colorLerper, x => colorLerper = x, target, _dur)
        .OnUpdate(() =>
        {
            rend.material.SetColor("_Color", colorLerper);
        });
    }
    private void SetDamageColorOnce(Color targetColor, float _dur)
    {
        StopColorTwn();
        rend.material.SetColor("_Color", targetColor);
        Color colorLerper = rend.material.GetColor("_Color");
        colorTwn = DOTween.To(() => colorLerper, x => colorLerper = x, oriColor, _dur)
        .OnUpdate(() =>
        {
            rend.material.SetColor("_Color", colorLerper);
        });
    }
    private void ReturnStatusColor(float _dur)
    {
        StopColorTwn();
        Color colorLerper = rend.material.GetColor("_Color");
        colorTwn = DOTween.To(() => colorLerper, x => colorLerper = x, oriColor, _dur)
        .OnUpdate(() =>
        {
            rend.material.SetColor("_Color", colorLerper);
        });
    }
    private IEnumerator ColorMaskFlash(Color targetColor, float  duration)
    {
        bool launching = false;
        float timer = 0;
        StopColorTwn();

        while(timer <= duration)
        {
            timer += Time.deltaTime;
            if(!launching)
            {
                launching = true;
                Color colorLerper = rend.material.GetColor("_Color");
                colorTwn = DOTween.To(() => colorLerper, x => colorLerper = x, targetColor, duration/8)
                .OnUpdate(() =>
                {
                    rend.material.SetColor("_Color", colorLerper);
                })
                .OnComplete(() => 
                {
                    colorTwn = DOTween.To(() => colorLerper, x => colorLerper = x, Color.Lerp(oriColor, targetColor, 0.6f), duration/8)
                    .OnUpdate(() =>
                    {
                        rend.material.SetColor("_Color", colorLerper);
                    })
                    .OnComplete(() => 
                    {
                        launching = false;
                    });
                });
            }
            
            yield return null;
        }
    }
    private IEnumerator DoActionPeriod(DoTData _data, Action action)
    {
        float counterInPeriod = 0;
        float counterPerUnit = 0;
        var firePs = statusVfxDic["Burning"].vfx.main;
        firePs.duration = _data.Duration;
        statusVfxDic["Burning"].vfx.gameObject.SetActive(true);

        if(dataBase.HP > 0)
            StartCoroutine(ColorMaskFlash(statusVfxDic["Burning"].EffectColor, _data.Duration));

        while(counterInPeriod <= _data.Duration)
        {
            if(counterPerUnit >= _data.TimeUnit)
            {
                counterPerUnit = 0;
                action();
            }
            counterPerUnit += Time.deltaTime;
            counterInPeriod += Time.deltaTime;
            yield return null;
        }
        dataBase.RemoveEnum(DeBuff.Burning);

        if(dataBase.HP > 0)
            ReturnStatusColor(0.5f);

        yield return new WaitForSeconds(1);
        statusVfxDic["Burning"].vfx.gameObject.SetActive(false);
    }
    private IEnumerator DisappearAfterDead(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        Color currentColor = rend.material.GetColor("_Color");
        SetColorTo(new Color(0, 0, 0, 0), 1);
        Debug.Log("Start Disappear !");
    }
}

[Serializable]
public class StatusFeedBack
{
    public string name;
    public Color EffectColor;
    public Color DeadColor;
    public ParticleSystem vfx;
}
