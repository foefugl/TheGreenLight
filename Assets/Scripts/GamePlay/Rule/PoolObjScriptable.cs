﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjScriptable : ScriptableObject
{
    public string ObjectName;
    public int ID;
    public float lifeTime;
    public GameObject basePrefab;
}
