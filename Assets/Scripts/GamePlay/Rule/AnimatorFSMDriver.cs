﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFSMDriver : MonoBehaviour
{
    public Animator animator;
    private EnemyFSM enemyFSM;
    private CreatureDataBase dataBase;

    private void Start()
    {
        enemyFSM = GetComponent<EnemyFSM>();
        dataBase = GetComponent<CreatureDataBase>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("Hp", dataBase.HP);
        animator.SetInteger("VisibleState", (int)enemyFSM.visibleState);
        animator.SetFloat("PlayerLookAngle", Vector3.Angle(enemyFSM.DirPlayerAim, (enemyFSM.PlayerPosAdjusted - transform.position).normalized));
    }
    public void SetSpecialPattern(int _code)
    {
        animator.SetTrigger("Mode" + _code.ToString());
    }
    public void SetHearFlag()
    {
        if(animator.GetCurrentAnimatorStateInfo(0).IsName("DefaultState") && animator.GetCurrentAnimatorStateInfo(0).IsName("Alerting"))
        {
            animator.CrossFade("Alerting", 0.01f);
        }
    }
    public void SetDeadFlag()
    {
        animator.SetBool("Dead", true);
    }
}
