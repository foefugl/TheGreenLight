﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjectEntity : MonoBehaviour
{
    public PoolObjScriptable objectSetting;
    private Vector3 oriScale;
    protected Action LifeTimeEndAction;

    private void Start()
    {
        gameObject.name = '#' + objectSetting.name;
        Initial();  
    }
    protected virtual void Initial()
    {
        gameObject.SetActive(false);
        oriScale = transform.lossyScale;
    }
    protected virtual void BeTaken()
    {
        
    }
    protected virtual void BeReturn()
    {
        StartCoroutine(LifeTimer(LifeTimeEndAction));
    }
    protected virtual IEnumerator LifeTimer(Action action)
    {
        yield return new WaitForSeconds(objectSetting.lifeTime);
        action();
    }
}
