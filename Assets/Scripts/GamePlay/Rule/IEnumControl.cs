﻿
public interface IEnumControl<T>
{
    void OnlyFlagEnum(T index);
    void ClearEnum();
    void RemoveEnum(T index);
    void AddEnum(T index);
    bool HasEnum(T index);
    void ToggleEnum(T index);
}
