﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CreatureDataBase : MonoBehaviour, IEnumControl<DeBuff>
{
    [SerializeField]
    protected float hp = 100;
    public float HpMaxValue;
    public float HP
    {
        get
        {
            return hp;
        }
        set
        {
            if(value <= 0)
                DeadEvents.Invoke();
            else if(value <= HpMaxValue/4)
                HpLowThanQuarter.Invoke(HpLowThanQuarter.GetPersistentEventCount());

            if(value < hp)
                InjuredEvents.Invoke();
            hp = Mathf.Clamp(value, 0, HpMaxValue);
        }
    }
    public float MoveSpeed = 1;
    [SerializeField]
    private DeBuff debuffStatus = DeBuff.None;
    public DeBuff DeBuff { get{ return debuffStatus; }}
    public UnityEvent InjuredEvents, DeadEvents;
    public UnityIntEvent HpLowThanQuarter;
    private void Awake()
    {
        HpMaxValue = hp;
        Initial();  
    }
    private void Update()
    {
        FrameUpdate();
    }
    public virtual void Initial()
    {

    }
    public virtual void FrameUpdate()
    {

    }

    public void RemoveEnum(DeBuff _target)
    {
        if(HasEnum(_target))
            debuffStatus &= ~_target;
        else
            Debug.LogWarning("Don't have this status : " + _target);
    }
    public void AddEnum(DeBuff _target)
    {
        if(!HasEnum(_target))
            debuffStatus |= _target;
        else
            Debug.LogWarning("Already have this type of status : " + _target);
    }
    public bool HasEnum(DeBuff _target)
    {
        if(_target == DeBuff.None)
            return false;
        else
            return  (debuffStatus & _target) != 0;
    }
    public void ToggleEnum(DeBuff _target)
    {
        debuffStatus ^= _target;
    }
    public void OnlyFlagEnum(DeBuff _target)
    {
        debuffStatus &= 0;
        debuffStatus |= _target;
    }
    public void ClearEnum()
    {
        debuffStatus &= 0;
    }
}
