﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Noiser : MonoBehaviour
{
    public float radius;
    public LayerMask effectLayer;

    public void MakeSound(float _assignRadius)
    {
        radius = _assignRadius;
        Collider[] colliders = Physics.OverlapSphere(transform.position, _assignRadius, effectLayer);
        foreach(var member in colliders)
        {
            var enemyBrain = member.gameObject.GetComponent(typeof(AnimatorFSMDriver)) as AnimatorFSMDriver;
            if(enemyBrain == null)
                enemyBrain =  member.gameObject.GetComponentInParent(typeof(AnimatorFSMDriver)) as AnimatorFSMDriver;
            if(enemyBrain == null)
                enemyBrain = member.gameObject.GetComponentInChildren(typeof(AnimatorFSMDriver)) as AnimatorFSMDriver;
                
            if(enemyBrain != null)
                enemyBrain.SetHearFlag();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.magenta;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, transform.forward, 360, radius);
    }
#endif
}
