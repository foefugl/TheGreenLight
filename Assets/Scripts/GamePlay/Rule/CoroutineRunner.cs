﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;

public class CoroutineRunner : MonoBehaviour
{
    [SerializeField]
    private List<FSMStateCoroutinePair> coroutinePairsList;
    private Dictionary<StateMachineBehaviour, Coroutine> CoroutinesDic;
    private void Start()
    {
        CoroutinesDic = new Dictionary<StateMachineBehaviour, Coroutine>();
        coroutinePairsList = new List<FSMStateCoroutinePair>();
    }
    private void LateUpdate()
    {
        coroutinePairsList = coroutinePairsList.Where(item => item != null).ToList();
        CoroutinesDic = CoroutinesDic.Where(value => value.Value != null).ToDictionary(t => t.Key, t => t.Value);
    }
    public void DoCoroutine(FSMStateCoroutinePair sourceData)
    {
        var currentCoroutine = StartCoroutine(sourceData.coroutine);
        if(!CoroutinesDic.ContainsKey(sourceData.state))
        {
            coroutinePairsList.Add(sourceData);
            CoroutinesDic.Add(sourceData.state, currentCoroutine);
        }
    }
    public void KillCoroutine(FSMStateCoroutinePair sourceData)
    {
        if(CoroutinesDic.ContainsKey(sourceData.state))
        {
            Coroutine contentCor = CoroutinesDic[sourceData.state];
            coroutinePairsList.Remove(sourceData);
            StopCoroutine(contentCor);
            CoroutinesDic.Remove(sourceData.state);
        }
    }
}
[System.Serializable]
public class FSMStateCoroutinePair
{
    public StateMachineBehaviour state;
    public IEnumerator coroutine;
    public FSMStateCoroutinePair(StateMachineBehaviour _state, IEnumerator _coroutine)
    {
        state = _state;
        coroutine = _coroutine;
    }
}
