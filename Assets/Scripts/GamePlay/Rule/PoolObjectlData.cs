﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolObjectlData<T>
{
    public string name;
    public int amount;
    public Transform Cabinet;
    public List<T> Clip;

    public virtual T Take()
    {
        T answer = default(T);
        if(Cabinet.childCount == 0)
            return default(T);

        answer = Cabinet.GetChild(0).GetComponent<T>();
        return answer;
    }
    public virtual void Return(T target)
    {
        Debug.Log("Parent Return Func");
    }
}
