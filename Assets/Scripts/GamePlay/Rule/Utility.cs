﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public enum MonsterVisibleState
{
    UnVisible = 0,
    OnlyCanListen = 1,
    Visible = 2
}

public enum PlayerMotionState
{
    Walk,
    Run,
    Crouch
}
[Serializable]
public class DoTData
{
    public float Duration;
    public float TimeUnit;
    public float PerUnitDamage;
}
[Flags]
public enum DeBuff
{
    None = 0,
    Burning = 1,
    Poisoning = 2,
    ElectricShock = 4,
    Fear = 8,
    Weak = 16,
    Tired = 32,
}
public enum MotionSpeedStatus
{
    None = 0,
    Walk  = 1,
    Creep = 2,
    Run = 4,
    Exhausted = 8
}
[System.Serializable]
public class ValueBarSetting
{
    [SerializeField]
    private float max;
    public float Max { get { return max; } }
    [SerializeField]
    private float min;
    public float Min { get { return min; } }
    [SerializeField]
    private float amount;
    public float Amount 
    {
        get { return amount; }
        set
        {
            amount = Mathf.Clamp(value, min, max);
        }
    }
    private float recoverCounter = 0;
    private float recover = 0, lose = 0;
    public ValueBarSetting(float _amount, float _min, float _max, float _recover, float _lose)
    {
        amount = _amount;
        min = _min;
        max = _max;
        recover = _recover;
        lose = _lose;
    }
    public void FrameUpate()
    {
        recoverCounter = 0;
        Amount -= lose;
        Amount += recover;
    }
    IEnumerator DoActionPeriod(DoTData _data, Action action)
    {
        float counterInPeriod = 0;
        float counterPerUnit = 0;
        while(counterInPeriod <= _data.Duration)
        {
            if(counterPerUnit >= _data.TimeUnit)
            {
                counterPerUnit = 0;
                action();
            }
            counterPerUnit += Time.deltaTime;
            counterInPeriod += Time.deltaTime;
            yield return null;
        }
    }
}

[System.Serializable]
public class UnityIntEvent : UnityEvent<int>
{
    
}