﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbushState : AniFSMStateBase
{
    public float sneakDis = 5.45f;
    public LayerMask obstacleLayer;
    private Collider hideCollider;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        enemyBrain.ChangeViewData("Ambush");
        hideCollider = enemyBrain.currentRoom.ClosestObstacle(animator.transform.position);
        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, AmbushCoroutine());
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
    }
    IEnumerator AmbushCoroutine()
    {
        while(true)
        {
            currentAnimator.transform.forward = enemyBrain.DirToTarget;
            float disToTarget = enemyBrain.DisToTarget;
            if(hideCollider == null)
            {
                if(disToTarget <= 1)
                {
                    currentAnimator.SetTrigger("StateActionFinish");
                    break;
                }
                if(disToTarget >= sneakDis &&　Vector3.Angle(currentAnimator.transform.forward, enemyBrain.DirPlayerAim) < 45)
                {
                    currentAnimator.SetTrigger("StateActionFinish");
                    break;
                }
            }
            else
            {
                float dis = Vector3.Distance(enemyBrain.transform.position, hideCollider.transform.position);
                Vector3 dir = (new Vector3(hideCollider.transform.position.x, enemyBrain.PlayerPosAdjusted.y, hideCollider.transform.position.z) - enemyBrain.PlayerPosAdjusted).normalized;
                Vector3 hidePoint = hideCollider.ClosestPoint(hideCollider.transform.position + dir * dis);

                if(dis > 0.5f)
                    currentAnimator.SendMessage("SetDestin", hidePoint);
                
                if(disToTarget <= 1.5f)
                {
                    currentAnimator.SetTrigger("StateActionFinish");
                    // int randPattern = Random.Range(0, 2);
                    currentAnimator.SetBool("AttackPattern" + 0.ToString(), true);
                    break;
                }
                if(disToTarget >= sneakDis)
                {
                    currentAnimator.SetTrigger("StateActionFinish");
                    break;
                }
            }
            yield return null;
        }
    }
}
