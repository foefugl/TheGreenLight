﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : AniFSMStateBase
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        animator.SendMessage("AgentStopAtPoint");
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("AgentStopAtPoint");
    }
    // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }
}
