﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HideState : AniFSMStateBase
{
    public LayerMask obstacleLayer;
    private Vector3 hidePoint;
    private Collider hideCollider;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        Debug.Log("Enter Hide");
        enemyBrain.ChangeViewData("Ambush");
        currentAnimator.SendMessage("AgentStopAtPoint");
        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, HideCoroutine());
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
        // animator.SetBool("AttackPattern0", false);
        // animator.SetBool("AttackPattern1", false);
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // enemyBrain.ChangeViewData("Normal");
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
    }
    private IEnumerator HideCoroutine()
    {
        // Collider[] obstacles = Physics.OverlapSphere(currentAnimator.transform.position, enemyBrain.ViewDisLimit, obstacleLayer);
        Dictionary<float, Collider> disObstaclePair = new Dictionary<float, Collider>();
        Collider farthestObstacle = enemyBrain.currentRoom.FarthestObstacle(enemyBrain.PlayerPosAdjusted);
        Room randRoom = enemyBrain.currentRoom.SideRooms[Random.Range(0, enemyBrain.currentRoom.SideRooms.Length)];
        if(farthestObstacle == null)
            farthestObstacle = randRoom.FarthestObstacle(enemyBrain.PlayerPosAdjusted);

        if(farthestObstacle == null)
        {
            hidePoint = randRoom.doors[Random.Range(0, randRoom.doors.Length)].transform.position;
            currentAnimator.SendMessage("SetDestin", hidePoint);
            yield return new WaitUntil(() => enemyBrain.agent.velocity == Vector3.zero);
            currentAnimator.SetTrigger("StateActionFinish");
        }
        else
        {
            disObstaclePair.Add(Vector3.Distance(farthestObstacle.transform.position, enemyBrain.transform.position), farthestObstacle);
            Vector3 dir = (new Vector3(farthestObstacle.transform.position.x, enemyBrain.PlayerPosAdjusted.y, farthestObstacle.transform.position.z) - enemyBrain.PlayerPosAdjusted).normalized;
            float dis = Vector3.Distance(currentAnimator.transform.position, farthestObstacle.transform.position);
            hidePoint = farthestObstacle.ClosestPoint(farthestObstacle.transform.position + dir * dis);
            hideCollider = farthestObstacle;
            currentAnimator.SendMessage("SetDestin", hidePoint + dir * 0.25f);
            yield return new WaitUntil(() => Vector3.Distance(hidePoint, currentAnimator.transform.position) < 0.375f);
            currentAnimator.SetTrigger("StateActionFinish");

            // if(disObstaclePair.Count != 0)
            // {
                // var orderByDis = disObstaclePair.OrderByDescending(o => o.Key);
                // Collider colForHide = orderByDis.ElementAt(0).Value;
                // for(int i=0 ; i<orderByDis.Count() ; i++ ){
                //     if(i==0)
                //     Debug.DrawLine(currentAnimator.transform.position, orderByDis.ElementAt(i).Value.transform.position, Color.green);
                //     else
                //     Debug.DrawLine(currentAnimator.transform.position, orderByDis.ElementAt(i).Value.transform.position, Color.cyan);
                // }
            // }
        }
    }
}
