﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ChaseingState : AniFSMStateBase
{
    public float stopDis = 0.275f;
    public int AtkPatternIndex = 0;
    private Vector3 chasePoint;
    private bool reachEndPoint;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        reachEndPoint = false;
        chasePoint = enemyBrain.PlayerPosAdjusted;
        enemyBrain.ChangeViewData("Angry");
        
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(enemyBrain.visibleState == EnemyFSM.VisibleState.Visible)
        {
            chasePoint = enemyBrain.PlayerPosAdjusted - (enemyBrain.PlayerPosAdjusted - animator.transform.transform.position).normalized * 0.115f;
        }
        if(Vector3.Distance(animator.transform.position, chasePoint) <= stopDis)
        {
            if(!reachEndPoint){
                reachEndPoint = true;
                currentAnimator.SetTrigger("StateActionFinish");
            }
            return;
        }
        animator.SendMessage("SetDestin", chasePoint);
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("AgentStopAtPoint");
        enemyBrain.ChangeViewData("Normal");
        Debug.Log("Chaseing Exit");
        animator.SetBool("AttackPattern" + AtkPatternIndex.ToString(), false );
    }
}
