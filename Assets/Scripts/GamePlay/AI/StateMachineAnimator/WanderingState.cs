﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingState : AniFSMStateBase
{
    public float WanderDis = 4;
    private Vector3 resiPoint;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);

        resiPoint = animator.transform.position;
        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, WanderingCoroutine(animator));
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
    }
    IEnumerator WanderingCoroutine(Animator animator)
    {
        while(true)
        {
            float randTime = Random.Range(4.0f, 8.0f);
            Vector3 randPoint = Vector3.zero;

            if(enemyBrain.currentRoom != null)
                randPoint  = enemyBrain.currentRoom.RandomPointInRoom();
            else
            {
                float randAngle = Random.Range(0, 360) * Mathf.Rad2Deg;
                float randDis = Random.Range(0.75f, WanderDis);
                randPoint = new Vector3(Mathf.Cos(randAngle), 0, Mathf.Sin(randAngle)) * randDis + resiPoint;
            }
        
            animator.gameObject.SendMessage("SetDestin", randPoint + Vector3.up);
            yield return new WaitForSeconds(randTime);
        }
    }
}
