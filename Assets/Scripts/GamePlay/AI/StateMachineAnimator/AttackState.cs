﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : AniFSMStateBase
{
    public int AtkPatternIndex = 0;
    private EnemyAniDriver aniDriver;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        aniDriver = animator.GetComponent<EnemyAniDriver>();
        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, AttackCoroutine());
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyBrain.ChangeViewData("Angry");
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
        Debug.Log("Attack Exit");
        animator.SetBool("AttackPattern" + AtkPatternIndex.ToString(), false );
    }
    IEnumerator AttackCoroutine()
    {
        bool finishFlag = false;
        aniDriver.ShortAttack(() => { finishFlag = true;});
        yield return new WaitUntil(() => finishFlag);
        yield return new WaitForSeconds(0.125f);
        currentAnimator.SetTrigger("StateActionFinish");
    }
}
