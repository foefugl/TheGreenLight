﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AniFSMStateBase : StateMachineBehaviour
{
    protected HuminoidEnemy enemyBrain;
    protected Animator currentAnimator;
    protected FSMStateCoroutinePair fSMStateCoroutinePair;
    private bool hasBeenInitial = false;
    protected Tween rotTwn;
    
    protected void Initial(Animator _animator)
    {
        if(!hasBeenInitial)
        {
            hasBeenInitial = true;
            currentAnimator = _animator;
            enemyBrain =  _animator.GetComponent<HuminoidEnemy>();
        }
    }
    protected void LookAround(float unitDur)
    {
        Debug.Log("Start Look Arround !");

        rotTwn = currentAnimator.transform.DOBlendableLocalRotateBy(Vector3.up * 60, unitDur)
        .OnComplete(() => 
        {
            rotTwn = currentAnimator.transform.DOBlendableLocalRotateBy(Vector3.up * -120, unitDur * 2)
            .OnComplete(() =>
            {
                rotTwn = currentAnimator.transform.DOBlendableLocalRotateBy(Vector3.up * 60, unitDur)
                .OnComplete(() =>
                {
                    rotTwn = null;
                });
            });
        });
    }
}
