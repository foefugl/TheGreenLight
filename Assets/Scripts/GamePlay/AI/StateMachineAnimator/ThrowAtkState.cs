﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAtkState : AniFSMStateBase
{
    public int AtkPatternIndex = 1;
    public float aimingTime = 0.5f;
    private EnemyAniDriver aniDriver;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Initial(animator);
        aniDriver = animator.GetComponent<EnemyAniDriver>();
        currentAnimator.SendMessage("AgentStopAtPoint");
        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, ThrowAtkCoroutine());
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
        animator.SetBool("AttackPattern" + AtkPatternIndex.ToString(), false );
    }
    IEnumerator ThrowAtkCoroutine()
    {
        float aimingTimer = 0;
        bool finishFlag = false;
        while(true)
        {
            aimingTimer += Time.deltaTime;
            if(aimingTimer <= aimingTime)
            {
                currentAnimator.transform.LookAt(enemyBrain.PlayerPosAdjusted);
                yield return null;
            }
            else
            {
                aniDriver.ThrowAttack(() => {finishFlag = true;});
                yield return new WaitUntil(() => finishFlag);
                yield return new WaitForSeconds(0.125f);
                break;
            }
        }
        currentAnimator.SetTrigger("StateActionFinish");
    }
}
