﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AlertingState : AniFSMStateBase
{
    public int AtkPatternCount = 2;
    public float rotateSpd = 2.25f;
    private Vector3 voiceSourceDir, voiceSourcePos;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Enter Alerting");
        Initial(animator);

        fSMStateCoroutinePair = new FSMStateCoroutinePair(this, AlartingCoroutine(enemyBrain.PlayerPosAdjusted));
        animator.SendMessage("DoCoroutine", fSMStateCoroutinePair);
        enemyBrain.ChangeViewData("Alerting");

        for(int i=0 ; i<AtkPatternCount ; i++){
            animator.SetBool("AttackPattern" + i.ToString(), false);
        }

        if(rotTwn != null){
            if(rotTwn.IsPlaying())
                rotTwn.Kill();
        }
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(enemyBrain.visibleState == EnemyFSM.VisibleState.OnlyCanListen)
        {
            voiceSourcePos = enemyBrain.PlayerPosAdjusted;
            voiceSourceDir = enemyBrain.DirToTarget;
        }
        else if(enemyBrain.visibleState == EnemyFSM.VisibleState.Visible)
        {
            int randSeed = Random.Range(0, AtkPatternCount);
            animator.SetBool("AttackPattern" + randSeed.ToString(), true);
            // animator.SetBool("AttackPattern" + 1.ToString(), true);
            // animator.SetBool("AttackPattern" + 0.ToString(), true);
        }
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SendMessage("KillCoroutine", fSMStateCoroutinePair);
        enemyBrain.ChangeViewData("Normal");
    }
    IEnumerator AlartingCoroutine(Vector3 targetPos)
    {
        voiceSourceDir = enemyBrain.DirToTarget;
        voiceSourcePos = targetPos;
        float disToVoiceSource = 0, angleToVoiceSource = 0;
        disToVoiceSource = Vector3.Distance(currentAnimator.transform.position, voiceSourcePos);
        angleToVoiceSource = Vector3.Angle(currentAnimator.transform.forward, voiceSourceDir);

        while(true)
        {   
            if(enemyBrain.visibleState == EnemyFSM.VisibleState.Visible){
                currentAnimator.SetTrigger("StateActionFinish");
                yield break;
            }
            while(angleToVoiceSource > 0.25f)
            {
                currentAnimator.SendMessage("AgentStopAtPoint");
                angleToVoiceSource = Vector3.Angle(currentAnimator.transform.forward, voiceSourceDir);
                Quaternion lookAtRot = Quaternion.LookRotation(voiceSourceDir, Vector3.up);
                currentAnimator.transform.rotation = Quaternion.Lerp(currentAnimator.transform.rotation, lookAtRot, Time.deltaTime * rotateSpd);
                yield return null;
            }
            while(disToVoiceSource > 0.25f && angleToVoiceSource <= 0.25f)
            {
                disToVoiceSource = Vector3.Distance(currentAnimator.transform.position, voiceSourcePos);
                currentAnimator.SendMessage("SetDestin", voiceSourcePos);
                yield return null;
            }
            while(disToVoiceSource <= 0.25f)
            {
                currentAnimator.SendMessage("AgentStopAtPoint");
                LookAround(1f);
                yield return new WaitForSeconds(1 * 4);
                currentAnimator.SetTrigger("StateActionFinish");
            }
        }
    }
}
