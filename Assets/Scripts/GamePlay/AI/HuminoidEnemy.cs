﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;
using DG.Tweening;

[RequireComponent(typeof(EnemyAniDriver))]
[RequireComponent(typeof(Noiser))]
public class HuminoidEnemy : EnemyFSM
{
    public Transform nextPoint;
    public enum FSMState
    {
        Chasing,
        Alerting,
        Wandering,
        Attack,
        Hiding,
        Ambush, 
        Dead
    }
    private FSMState state;
    public FSMState fsmState 
    {
        get
        {
            return state;
        }
        set
        {
            if(state != value){
                resiPoint = transform.position;
            }
            state = value;
        }
    }
    public float WanderDis = 4;
    private float counter = 0;
    public NavMeshAgent agent;
    protected EnemyAniDriver aniDriver;
    protected AnimatorFSMDriver animatorFSMDriver;
    private LayerMask obstacleLayer;
    private Noiser noiser;
    private Collider hideCollider;
    private Tween rotTwn;
    private Vector3 resiPoint, chasePoint, hidePoint;
    private float normalSpd, normalViewDis, normalAngle;
    private bool coroutinePlaying = false;
    protected override void Initialize()
    {
        agent             = GetComponent<NavMeshAgent>();
        noiser            = GetComponent<Noiser>();
        atkCause          = GetComponent<AttackCause>();
        aniDriver         = GetComponent<EnemyAniDriver>();
        animatorFSMDriver = GetComponent<AnimatorFSMDriver>();
        damageTaker       = GetComponent<DamageTaker>();
        dataBase          = GetComponent<CreatureDataBase>();
        playerTransform   = GameObject.FindGameObjectWithTag("Player").transform;
        playerAimTransforms = playerTransform.Find("[ Rotator ]");
        obstacleLayer     = LayerMask.GetMask("Obstacle");

        resiPoint = transform.position;
        normalSpd = agent.speed;
        normalViewDis = ViewDisLimit;
        normalAngle = ViewAngle;
    }
    protected override void FSMUpdate()
    {
        base.FSMUpdate();
    }
    public void ChangeViewData(string code)
    {
        switch(code)
        {
            case "Normal" : 
            agent.speed = normalSpd;
            ViewDisLimit = normalViewDis;
            ViewAngle = normalAngle;
            break;
            case "Angry" :
            agent.speed = normalSpd * 1.825f;
            ViewDisLimit = normalViewDis * 1.25f;
            ViewAngle = 360;
            break;
            case "Ambush" :
            agent.speed = normalSpd * 1.625f;
            ViewDisLimit = normalViewDis * 1.5f;
            ViewAngle = normalAngle;
            break;
            case "Alerting" :
            agent.speed = normalSpd;
            ViewDisLimit = normalViewDis * 1.25f;
            ViewAngle = normalAngle;
            break;
        }
    }
    public void Hitted()
    {
        Vector3 noisePos = noisePosTrans.position +　agent.velocity.normalized * 0.25f + -Camera.main.transform.forward * 5;
        PSObjectPool.Instance.Take(1, noisePos, transform.rotation.eulerAngles, true);
        noiser.MakeSound(4f);
        transform.DOJump(transform.position - transform.forward * 0.35f, 0.225f, 1, 0.5f);
    }
    private void AgentStopAtPoint() 
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 5, floorLayer))
        {
            agent.SetDestination(hit.point);
        }
    }
    private void SetDestin(Vector3 targetPos)
    {
        RaycastHit hit;
        if(Physics.Raycast(targetPos + Vector3.up, Vector3.down, out hit, 10, floorLayer)){
            NavMeshPath path = new NavMeshPath();
            if(agent.CalculatePath(hit.point, path))
            {
                agent.SetPath(path);
                nextPoint.position = hit.point;
            }
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.green;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, transform.forward, 360, ListenRadius);

        Handles.color = Color.green;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, transform.forward, 360, ListenRadius/3);

        Handles.color = Color.cyan;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, Quaternion.Euler(Vector3.up * -ViewAngle/2) * transform.forward, ViewAngle, ViewDisLimit);

        Handles.color = Color.red;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(resiPoint, transform.up, transform.forward, 360, WanderDis);

        if(!EditorApplication.isPlaying)
            return;

        RaycastHit hit;
        bool isHit = Physics.BoxCast(transform.position, Vector3.one * 0.125f, DirToTarget, out hit, transform.rotation, 100);
        if(isHit)
        {
            Gizmos.color = Color.red;
            
            Gizmos.DrawLine(transform.position, transform.position + DirToTarget * hit.distance);
            Gizmos.DrawWireCube(transform.position + DirToTarget * hit.distance, Vector3.one * 0.125f * 2);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, DirToTarget * 100);
        }
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(hidePoint, 0.1f);

        Handles.Label((transform.position + PlayerPosAdjusted)/2, "Dis : " +  DisToTarget.ToString());
        Handles.Label((transform.position + PlayerPosAdjusted)/2 - Vector3.up * 0.15f, "Angle : " +  Vector3.Angle(transform.forward, DirPlayerAim).ToString());
        Gizmos.DrawLine(transform.position, PlayerPosAdjusted);
    }
#endif
}
