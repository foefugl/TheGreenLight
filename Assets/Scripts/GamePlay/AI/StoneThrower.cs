﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallLauncher))]
public class StoneThrower : MonoBehaviour
{
    private Rigidbody playerRB;
    public float predictDis = 0.5f;
    public Collider parentCollider, stoneCollider;
    public Stone stone;
    private HuminoidEnemy aiBrain;
    private BallLauncher ballLauncher;
    private void Start()
    {
        aiBrain        = GetComponentInParent<HuminoidEnemy>();
        ballLauncher   = GetComponent<BallLauncher>();
        playerRB = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        Physics.IgnoreCollision(parentCollider, stoneCollider);
    }
    public void ThrowStone()
    {
        stone.transform.parent = null;
        stone.VisibleBall();
        BallLauncher.LaunchData data = ballLauncher.LaunchDataReturn(stone.rb, aiBrain.PlayerPosAdjusted);
        ballLauncher.Launch(stone.rb, aiBrain.PlayerPosAdjusted + playerRB.velocity * data.timeToTarget + aiBrain.DirToTarget);
    }
}
