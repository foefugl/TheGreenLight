﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AttackCause))]
public class Stone : MonoBehaviour
{
    public AttackCause attackCause;

    public Rigidbody rb;
    private Transform holder;
    private Vector3 initialLocalPos;
    private MeshRenderer rend;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<MeshRenderer>();
        rend.enabled = false;
        holder = transform.parent;  
        initialLocalPos = transform.localPosition;

    }
    public void VisibleBall()
    {
        rend.enabled = true;
    }
    private void OnCollisionEnter(Collision other) 
    {
        Return();
        if(!other.transform.CompareTag("Player"))
            return;

        DamageTaker playerDamageTaker = other.gameObject.GetComponent<DamageTaker>();
        if(playerDamageTaker == null)
            playerDamageTaker = other.gameObject.GetComponentInParent<DamageTaker>();
        if(playerDamageTaker == null)
            playerDamageTaker = other.gameObject.GetComponentInChildren<DamageTaker>();
        
        attackCause.SendAtkMessage(playerDamageTaker);
        
    }
    private void Return()
    {
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;
        rb.useGravity = false;
        transform.SetParent(holder);
        rend.enabled = false;
        transform.localPosition = initialLocalPos;
    }
}
