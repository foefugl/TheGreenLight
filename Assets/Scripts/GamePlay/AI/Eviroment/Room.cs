﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(BoxCollider))]
[ExecuteInEditMode]
public class Room : MonoBehaviour
{
    public Color roomColor;
    public LayerMask effectLayer, obstacleLayer, roomLayer;
    public Vector2 Range;
    public Door[] doors;
    private BoxCollider roomTrigger;
    public List<EnemyFSM> enemys;
    public Collider[] roomObstacles;
    public Room[] SideRooms;
    private void Awake()
    {
        enemys        = new List<EnemyFSM>();
        effectLayer   = LayerMask.GetMask("Creature");
        obstacleLayer = LayerMask.GetMask("Obstacle");
        roomLayer     = LayerMask.GetMask("Room");
    }

    // Start is called before the first frame update
    void Start()
    {
        roomTrigger = GetComponent<BoxCollider>();
        roomTrigger.size = new Vector3(Range.x * 2 , transform.lossyScale.y, Range.y * 2);
        roomTrigger.center = Vector3.down * transform.localScale.y/2;
        
        CollectEnemyLocalInitial();
        CollectObstaclesLocalInitial();
        CollectSideRoom();
    }
    private void CollectEnemyLocalInitial()
    {
        Collider[] collectColliders = Physics.OverlapBox( transform.position + roomTrigger.center, roomTrigger.size / 2, Quaternion.identity, effectLayer);
        for(int i=0 ; i<collectColliders.Length ; i++)
        {
            EnemyFSM currentDetect = collectColliders[i].gameObject.GetComponent<EnemyFSM>();
            if(currentDetect == null)
                currentDetect = collectColliders[i].gameObject.GetComponentInChildren<EnemyFSM>();
            if(currentDetect == null)
                currentDetect = collectColliders[i].gameObject.GetComponentInParent<EnemyFSM>();
            if(currentDetect != null){
                currentDetect.currentRoom = this;
                enemys.Add(currentDetect);
            }
        }
    }
    private void CollectSideRoom()
    {
        Collider[] sideRoomsCollider = Physics.OverlapBox( transform.position + roomTrigger.center, roomTrigger.size, Quaternion.identity, roomLayer);
        
        if(sideRoomsCollider.Length == 0)
            return;
        
        List<Room> temp = new List<Room>();
        for(int i=0 ; i<sideRoomsCollider.Length ; i++)
        {
            Room roomData = sideRoomsCollider[i].GetComponent(typeof(Room)) as Room;
            if(roomData != null && roomData != this)
                temp.Add(roomData);
        }
        SideRooms = temp.ToArray();
    }
    private void CollectObstaclesLocalInitial()
    {
        roomObstacles = Physics.OverlapBox( transform.position + roomTrigger.center, roomTrigger.size / 2, Quaternion.identity, obstacleLayer);
    }
    public bool IsTransfromInRoom(Transform target)
    {
        bool xValueConform = Range.x > Mathf.Abs(target.position.x - transform.position.x);
        bool zValueConform = Range.y > Mathf.Abs(target.position.z - transform.position.z);
        return xValueConform & zValueConform;
    }
    public Collider FarthestObstacle(Vector3 demandPos)
    {
        Dictionary<float, Collider> disColliderDic = new Dictionary<float, Collider>();
        for(int i=0 ; i<roomObstacles.Length ; i++){
            disColliderDic.Add(Vector3.Distance(demandPos, roomObstacles[i].transform.position), roomObstacles[i]);
        }
        var afterOrder = disColliderDic.OrderByDescending(o => o.Key).ToList();
        return afterOrder.FirstOrDefault().Value;
    }
    public Collider ClosestObstacle(Vector3 demandPos)
    {
        Dictionary<float, Collider> disColliderDic = new Dictionary<float, Collider>();
        for(int i=0 ; i<roomObstacles.Length ; i++){
            disColliderDic.Add(Vector3.Distance(demandPos, roomObstacles[i].transform.position), roomObstacles[i]);
        }
        var afterOrder = disColliderDic.OrderBy(o => o.Key).ToList();
        return afterOrder.FirstOrDefault().Value;
    }

    public Vector3 RandomPointInRoom()
    {
        float randX = Random.Range(0.0f, Range.x) - Range.x/2;
        float randZ = Random.Range(0.0f, Range.y) - Range.y/2;
        return new Vector3(transform.position.x + randX, transform.position.y, transform.position.z + randZ);
    }

    private void OnTriggerEnter(Collider other) 
    {
        EnemyFSM currentDetect = other.gameObject.GetComponent<EnemyFSM>();
        if(currentDetect == null)
            currentDetect = other.gameObject.GetComponentInChildren<EnemyFSM>();
        if(currentDetect == null)
            currentDetect = other.gameObject.GetComponentInParent<EnemyFSM>();
        if(currentDetect == null)
            return;
        
        currentDetect.currentRoom = this;
        if(!enemys.Contains(currentDetect))
            enemys.Add(currentDetect);    
    }
    private void OnTriggerExit(Collider other)
    {
        EnemyFSM currentDetect = other.gameObject.GetComponent<EnemyFSM>();
        if(currentDetect == null)
            currentDetect = other.gameObject.GetComponentInChildren<EnemyFSM>();
        if(currentDetect == null)
            currentDetect = other.gameObject.GetComponentInParent<EnemyFSM>();
        if(currentDetect == null)
            return;

        // currentDetect.currentRoom = null;
        if(enemys.Contains(currentDetect))
            enemys.Remove(currentDetect);
            
    }
    private void LateUpdate() 
    {
        enemys = enemys.Where(item => item != null).ToList();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = roomColor;
        Vector3 pos = transform.position;

        Vector3[] verts = new Vector3[]
        {
            new Vector3(pos.x - Range.x, pos.y, pos.z - Range.y),
            new Vector3(pos.x - Range.x, pos.y, pos.z + Range.y),
            new Vector3(pos.x + Range.x, pos.y, pos.z + Range.y),
            new Vector3(pos.x + Range.x, pos.y, pos.z - Range.y)
        };
        Handles.DrawSolidRectangleWithOutline(verts, new Color(0.5f, 0.5f, 0.5f, 0.04f), new Color(0, 0, 0, 1)); 

        if(EditorApplication.isPlaying)
        {
            Gizmos.color = Color.red;
            Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.35f);
            Gizmos.DrawWireCube(transform.position + roomTrigger.center,  roomTrigger.size);
        }
    }
#endif
}
