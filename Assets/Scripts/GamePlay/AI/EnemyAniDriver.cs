﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class EnemyAniDriver : MonoBehaviour
{
    public Animator animator;
    public NavMeshAgent agent;
    private Action AttackFinishAction;

    // Update is called once per frame
    void Update()
    {
        if(agent.velocity.magnitude <= 0.165f)
        {
            animator.SetFloat("hSpeed", 0);
            animator.SetFloat("vSpeed", 0);

            if(transform.forward.z > 0)
            {
                animator.SetFloat("hLastDir", 0);
                animator.SetFloat("vLastDir", 1);    
            }
            else if(transform.forward.x > 0)
            {
                animator.SetFloat("hLastDir", 1);
                animator.SetFloat("vLastDir", 0);
            }
            else if(transform.forward.x <= 0)
            {
                animator.SetFloat("hLastDir", -1);
                animator.SetFloat("vLastDir", 0);
            }
            else if(transform.forward.z <= 0)
            {
                animator.SetFloat("hLastDir", 0);
                animator.SetFloat("vLastDir", -1);
            }
        }
        else
        {
            animator.SetFloat("hSpeed", agent.velocity.x);
            animator.SetFloat("vSpeed", -agent.velocity.z);
        }
    }
    public void ShortAttack(Action _endAction)
    {
        StartCoroutine(PlayAndWaitForAnim("Attack", _endAction));
    }
    public void ThrowAttack(Action _endAction)
    {
        StartCoroutine(PlayAndWaitForAnim("ThrowAttack", _endAction));
    }
    public void SetDead()
    {
        Debug.Log("Dead Animation");
        StartCoroutine(PlayAndWaitForAnim("Dead", () => {Debug.Log("Null Complete Action.");}));
    }
    public void SetDead(Action _endAction)
    {
        Debug.Log("Dead Animation");
        StartCoroutine(PlayAndWaitForAnim("Dead", _endAction));
    }
    private IEnumerator PlayAndWaitForAnim(string stateName, Action EndStepEvent)
    {
        animator.CrossFade(stateName, 0.05f);
        // animator.GetCurrentAnimatorStateInfo(0).IsName(stateName) || 
        while((animator.GetCurrentAnimatorStateInfo(0).normalizedTime) % 1 < 0.9f)
            yield return null;
        EndStepEvent();
    }
}
