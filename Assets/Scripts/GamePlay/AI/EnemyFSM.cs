﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AttackCause))]
[RequireComponent(typeof(DamageTaker))]
[RequireComponent(typeof(CreatureDataBase))]
public class EnemyFSM : FSM
{
    public enum VisibleState
    {
        UnVisible = 0,
        OnlyCanListen = 1,
        Visible = 2
    }
    public VisibleState visibleState = VisibleState.UnVisible;
    public Transform noisePosTrans;
    public Room currentRoom;
    public LayerMask ViewDetectLayer;
    public LayerMask floorLayer;
    public float ViewAngle = 45;
    public float ViewDisLimit = 6.5f;
    public float ListenRadius = 4;
    protected CreatureDataBase dataBase;
    protected AttackCause atkCause;
    protected DamageTaker damageTaker;
    protected Transform playerAimTransforms;
    public Vector3 PlayerPosAdjusted 
    { 
        get 
        { 
            return new Vector3(playerTransform.position.x, transform.position.y, playerTransform.position.z);
        }
    }
    public Vector3 DirToTarget
    {
        get
        {
            return (PlayerPosAdjusted - transform.position).normalized;
        }
    }
    public float DisToTarget
    {
        get
        {
            return Vector3.Distance(PlayerPosAdjusted, transform.position);
        }
    }
    public Vector3 DirPlayerAim
    {
        get
        {
            return playerAimTransforms.forward;
        }
    }
    public virtual void HearNoise(Transform fakePlayer)
    {
        
    }
    protected override void FSMUpdate()
    {
        visibleState = DetectStateUpdate();
    }

    public VisibleState DetectStateUpdate()
    {
        float   dis = DisToTarget;
        

        if(dis <= ViewDisLimit)
        {
            float angle = Vector3.Angle(transform.forward, DirToTarget);
            if(angle > (ViewAngle/2))
            {
                if(playerTransform.GetComponent<PlayerDataBase>().HasEnum(MotionSpeedStatus.Creep))
                {
                    if(dis > ViewDisLimit/3)
                        return VisibleState.UnVisible;
                    else
                        return VisibleState.OnlyCanListen;
                }
                else
                {
                    if(currentRoom.IsTransfromInRoom(playerTransform))
                        return VisibleState.OnlyCanListen;
                    else
                        return VisibleState.UnVisible;
                }
            }
            else
            {
                RaycastHit hit;
                Physics.BoxCast(transform.position, Vector3.one * 0.125f, DirToTarget, out hit, transform.rotation, dis, ViewDetectLayer);
                if(hit.collider == null)
                    return visibleState;
                if(hit.collider.gameObject != playerTransform.gameObject)
                    return VisibleState.UnVisible;

                return VisibleState.Visible;
            }
        }
        else
            return VisibleState.UnVisible;
    }
}
