﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AniEventBridge : MonoBehaviour
{
    public UnityEvent atkEvent, throwAtkEvent;
    public void AttackEvent()
    {
        atkEvent.Invoke();
    }
    public void ThrowAtkEvent()
    {
        throwAtkEvent.Invoke();
    }
}