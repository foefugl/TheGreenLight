﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class AttackCause : MonoBehaviour
{
    public bool atkFlag = false;
    public LayerMask effectLayer;
    public float damageValue;
    public float distOffset = 0.5f;
    public Vector3 atkCubeSize = Vector3.one;
    public UnityEvent shortAtkEvent;
    private Color currentColor = Color.yellow;
    public void ShortAttack()
    {
        if(atkFlag)
            return;

        var colliders = Physics.OverlapBox(transform.position + transform.forward * distOffset, atkCubeSize * 0.5f, transform.rotation, effectLayer);
        shortAtkEvent.Invoke();
        foreach(var member in colliders)
        {
            DamageTaker damTaker = member.gameObject.GetComponent<DamageTaker>();
            if(damTaker == null)
                damTaker = member.gameObject.GetComponentInParent<DamageTaker>();
            if(damTaker == null)
                damTaker = member.gameObject.GetComponentInChildren<DamageTaker>();
            if(damTaker != null)
            { 
                damTaker.ShortDamage(Color.red, damageValue);
            }
        }
        StartCoroutine(VisualizeChangeColor(10));
    }
    public void SendAtkMessage(DamageTaker damageTaker)
    {
        damageTaker.ShortDamage(Color.red, damageValue);
    }
    IEnumerator VisualizeChangeColor(int frameCount)
    {
        int counter = 0;
        atkFlag = true;
        while(counter < frameCount)
        {
            counter ++;
            currentColor = Color.red;
            yield return null;
        }
        currentColor = Color.yellow;
        atkFlag = false;
    }
    private void OnDrawGizmos() 
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = currentColor;
        Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.35f);
        Gizmos.DrawWireCube(Vector3.forward * distOffset, atkCubeSize);
    }
}