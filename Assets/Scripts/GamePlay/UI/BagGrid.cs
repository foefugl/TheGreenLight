﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BagGrid : MonoBehaviour, IPointerClickHandler
{
    public bool filled;
    public PackSystem beLongPackSystem;
    private EventSystem eventSystem;
    private void Awake() 
    {
        eventSystem = FindObjectOfType<EventSystem>();    
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if(SimpleMoveSprite.Instance.ItemUISelect != null && SimpleMoveSprite.Instance.ItemUISelect.GetComponent<PackItemData>().doubeSelect)
        {
            PackItemData packItemData = SimpleMoveSprite.Instance.ItemUISelect.GetComponent<PackItemData>();
            packItemData.doubeSelect = false;
            PointerEventData fakeData = new PointerEventData(eventSystem);
            fakeData.pointerEnter = eventData.pointerEnter;
            fakeData.selectedObject = SimpleMoveSprite.Instance.ItemUISelect;
            packItemData.selectImage = SimpleMoveSprite.Instance.ItemUISelect.GetComponent<Image>();
            packItemData.StorageItem(fakeData);
        }
    }
}
