﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolBagGrid : BagGrid
{
    public Image selectImage;
    public void SetSelectFrame(bool _onoff)
    {
        selectImage.gameObject.SetActive(_onoff);
    }
}
