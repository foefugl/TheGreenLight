﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PackItemData : UIElementInteract
{
    public PackSystem beLongPackSystem;
    public Image itemImage;
    public Text itemCountText;
    private List<ItemObject> itemPackList;
    
    private int currentCount;
    private void Awake() 
    {
        itemPackList = new List<ItemObject>();
        eventSystem = FindObjectOfType<EventSystem>();
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas").transform;
    }
    public List<ItemObject> CurrentItemList
    {
        get
        {
            return itemPackList;
        }
    }

    public void UpdateGridContent(ItemObject _itemObj, int count)
    {
        if(!CurrentItemList.Contains(_itemObj))
            CurrentItemList.Add(_itemObj);

        itemImage.sprite = _itemObj.itemData.sprite;
        itemCountText.text = count.ToString();
        currentCount = count;
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        StorageItem(eventData);
    }
    public override void ClearData()
    {
        itemPackList = new List<ItemObject>();
        itemImage.sprite = null;
        itemCountText.text = 0.ToString();
        currentCount = 0;
    }
    public void StorageItem(PointerEventData eventData)
    {
        doubeSelect = false;
        if(eventData.pointerEnter == null || eventData.selectedObject == null)
        {
            // 丟置半空中
            Debug.Log("丟置半空中");
            for(int i=0 ; i<CurrentItemList.Count ; i++){
                beLongPackSystem.RemoveFromBag(CurrentItemList[i], true);
            }
            
            if(currentCount != 0)
            {
                originGrid.filled = true;
                transform.parent = originGrid.transform;
                eventData.selectedObject.transform.localPosition = Vector2.zero;
            }
            else
            {
                originGrid.filled = false;
            }
            
            selectImage.raycastTarget = true;
            SimpleMoveSprite.Instance.ItemUISelect = null;
            return;
        }

        if(!eventData.pointerEnter.name.Contains("Grid"))
        {
            if(!eventData.pointerEnter.GetComponent<PackItemData>())
            {
                // 丟置半空中
                Debug.Log("無效Grid " + eventData.pointerEnter.name);
                transform.parent = originGrid.transform;
                eventData.selectedObject.transform.localPosition = Vector2.zero;
                selectImage.raycastTarget = true;
                SimpleMoveSprite.Instance.ItemUISelect = null;
            }
            else
            {
                // 和其他物件交換
                Debug.Log("和其他物件交換");
                Transform gridTrans = eventData.pointerEnter.transform.parent;
                BagGrid endPosGrid = gridTrans.GetComponent<BagGrid>();
                endPosGrid.filled = true;
                originGrid.filled = false;
                eventData.selectedObject.transform.SetParent(gridTrans);
                selectImage.raycastTarget = true;
                eventData.selectedObject.transform.localPosition = Vector3.zero;

                eventData.selectedObject = eventData.pointerEnter;
                eventData.selectedObject.transform.parent = mainCanvas;
                selectImage = eventData.selectedObject.GetComponent<Image>();
                selectImage.raycastTarget = false;
                PackItemData packItemData = eventData.selectedObject.GetComponent<PackItemData>();
                packItemData.doubeSelect = true;
                packItemData.originGrid = endPosGrid;
                SimpleMoveSprite.Instance.ItemUISelect = eventData.selectedObject;
            }
            return;
        }

        if(eventData.pointerEnter.name.Contains("Grid"))
        {
            BagGrid endPosGrid = eventData.pointerEnter.GetComponent<BagGrid>();
            if(endPosGrid == originGrid)
            {
                // 放回原先的格子
                Debug.Log("放回原先的格子");
                eventData.selectedObject.transform.SetParent(eventData.pointerEnter.transform);
            }
            else
            {
                if(!endPosGrid.filled)
                {
                    Debug.Log("放置新格子");
                    // 放置新的空格
                    if(endPosGrid.beLongPackSystem != beLongPackSystem)
                    {
                        var currentPackData = eventData.selectedObject.GetComponent<PackItemData>();
                        var oriPackSystem = currentPackData.beLongPackSystem;
                        for(int i=0 ; i<CurrentItemList.Count ; i++){
                            endPosGrid.beLongPackSystem.AddToBag(CurrentItemList[i]);
                        }
                        for(int i=0 ; i<CurrentItemList.Count ; i++){
                            beLongPackSystem.RemoveFromBag(CurrentItemList[i], false);
                        }
                        eventData.selectedObject.GetComponent<PackItemData>().beLongPackSystem = endPosGrid.beLongPackSystem;
                    }
                    originGrid.filled = false;
                    endPosGrid.filled = true;
                    eventData.selectedObject.transform.SetParent(eventData.pointerEnter.transform);
                }
            }
            selectImage.raycastTarget = true;
            eventData.selectedObject.transform.parent = eventData.pointerEnter.transform;
            eventData.selectedObject.transform.localPosition = Vector3.zero;
            SimpleMoveSprite.Instance.ItemUISelect = null;
        }
    }
}
