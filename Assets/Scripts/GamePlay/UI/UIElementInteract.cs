﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIElementInteract : MonoBehaviour
    ,IBeginDragHandler
    ,IDragHandler
    ,IEndDragHandler
    ,IPointerEnterHandler
    ,IPointerExitHandler
{
    public bool doubeSelect = false;
    [HideInInspector]
    public Image selectImage;
    protected BagGrid originGrid;
    protected Transform mainCanvas;
    protected EventSystem eventSystem;
    private void Update() 
    {
        if(doubeSelect)
        {
            transform.position = Input.mousePosition;
        }    
    }
    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        eventData.selectedObject = gameObject;
        originGrid = gameObject.transform.parent.GetComponent<BagGrid>();
        eventData.selectedObject.transform.parent =  mainCanvas;
        selectImage = eventData.selectedObject.GetComponent<Image>();
        selectImage.raycastTarget = false;
    }
    public virtual void OnDrag(PointerEventData eventData)
    {
        eventData.selectedObject.transform.position = Input.mousePosition;
    }
    public virtual void OnEndDrag(PointerEventData eventData)
    {
        
    }
    public virtual void ClearData()
    {
        
    }
    
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        // Debug.Log("Enter "+ gameObject.name);
    }
    public virtual void OnPointerExit(PointerEventData envetData)
    {
        // Debug.Log("Exit " + gameObject.name);
    }
}
