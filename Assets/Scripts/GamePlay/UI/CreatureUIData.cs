﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureUIData : MonoBehaviour
{
    public Image healthBar;
    protected CreatureDataBase creatureData;

    private void Awake()
    {
        Initial();
    }
    public void Update()
    {
        FrameUpdate();
    }
    protected virtual void Initial()
    {
        creatureData = GetComponent<CreatureDataBase>();
    }
    protected virtual void FrameUpdate()
    {
        healthBar.fillAmount = creatureData.HP / creatureData.HpMaxValue; 
    }
}
