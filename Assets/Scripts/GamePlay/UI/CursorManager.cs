﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CursorManager : MonoBehaviour, IEnumControl<CursorEnum>
{
    // public float  
    public CursorEnum cursorEnum = CursorEnum.None;
    
    public Image cursor;
    public Image chargeImage;
    public Image processingCircle;
    public CursorType[] cursorDataArray;
    private Dictionary<CursorEnum, Sprite> cursorSpriteDic;
    private Vector3 defaultScale;
    private float chargeProcessing, processCircleValue;
    
    [Header("[ Investigate Setting ]")]
    public LayerMask investigateLayer;
    [TagSelector]
    public string TagFilter = "";
    public float investigateTimeCost;
    private bool pointerAtObject, hasFinishInvest;
    private Treasury hitSelectedTreasure;

    private void Start()
    {
        Cursor.visible = false;
        defaultScale = transform.localScale;
        cursorSpriteDic = new Dictionary<CursorEnum, Sprite>();
        processingCircle.fillAmount = 0;
        for(int i=0 ; i<cursorDataArray.Length ; i++){
            cursorSpriteDic.Add(cursorDataArray[i].representEnum, cursorDataArray[i].sprite);
        }
    }

    private void LateUpdate()
    {
        cursor.transform.position = Input.mousePosition;
    }
    private void Update() 
    {
        // Physical Scan
        MouseScanWorldObject();

        // State Depend
        InvestigateStateDepender();
        ChargingStateDepender();

        // Change relative stuff by state
        ProcessCircle();
        AssignCursorSprite();

        // 
        InvestigateSystemCloseDefine();
    }
    private void ChargingStateDepender()
    {        
        if(SimpleMoveSprite.Instance.chargeCounter != 0)
        {
            if(!HasEnum(CursorEnum.Charging))
                OnlyFlagEnum(CursorEnum.Charging);
        }
        else
        {
            if(HasEnum(CursorEnum.Charging))
                RemoveEnum(CursorEnum.Charging);
        }
        
        float targetValue = Mathf.Clamp01(SimpleMoveSprite.Instance.chargeCounter / SimpleMoveSprite.Instance.chargeTime);
        chargeProcessing = Mathf.Lerp(chargeProcessing, targetValue, Time.deltaTime * 12.5f);

        chargeImage.color = new Color(chargeImage.color.r, chargeImage.color.g, chargeImage.color.b, chargeProcessing);
        chargeImage.transform.localScale = defaultScale * Mathf.Clamp01(1.35f - chargeProcessing);
    }
    private void InvestigateStateDepender()
    {
        if(HasEnum(CursorEnum.Charging))
            return;
        
        if(pointerAtObject && !HasEnum(CursorEnum.Investigate))
            OnlyFlagEnum(CursorEnum.Investigate);
        else if(!pointerAtObject && HasEnum(CursorEnum.Investigate))
            RemoveEnum(CursorEnum.Investigate);
    }
    private void AssignCursorSprite()
    {
        if(HasEnum(CursorEnum.Charging) || cursorEnum == 0)
            cursor.sprite = cursorSpriteDic[CursorEnum.None];
        else if(HasEnum(CursorEnum.Investigate))
            cursor.sprite = cursorSpriteDic[CursorEnum.Investigate];
    }
    private void ProcessCircle()
    {
        if(HasEnum(CursorEnum.Investigate) && Input.GetMouseButton(0) && !hasFinishInvest)
        {
            processCircleValue += Time.deltaTime;
            processCircleValue = Mathf.Clamp(processCircleValue, 0, investigateTimeCost);
            processingCircle.color = Color.white;
            if(processCircleValue == investigateTimeCost && !hasFinishInvest)
            {
                hasFinishInvest = true;
                float colorLerper = 1;
                OpenTreasuryContent();
                DOTween.To(() => colorLerper, x=>colorLerper=x, 0, 0.85f)
                .OnUpdate(() => 
                {
                    processingCircle.color *= colorLerper;
                })
                .OnComplete(() => 
                {
                    processCircleValue = 0;
                });
            }
        }
        else if((!Input.GetMouseButton(0) && !hasFinishInvest) || !HasEnum(CursorEnum.Investigate))
            processCircleValue -= Time.deltaTime;

        processingCircle.fillAmount = processCircleValue / investigateTimeCost;
    }
    private void InvestigateSystemCloseDefine()
    {
        // If player move close investigate bag
        if(hasFinishInvest && SimpleMoveSprite.Instance.IsMoving)
        {
            TreasureBagSystem.Instance.SetBackDataToObj(hitSelectedTreasure);
            TreasureBagSystem.Instance.ClearTreasureBagData();
            TreasureBagSystem.Instance.ToggleTreasureBag();
            hasFinishInvest = false;
            hitSelectedTreasure = null;
        }
    }
    private void OpenTreasuryContent()
    {
        hitSelectedTreasure.Replenishment();
    }
    private void MouseScanWorldObject()
    {
        if(hasFinishInvest){
            pointerAtObject = false;
            return;
        }
            
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 50, investigateLayer))
        {
            if(hit.collider.gameObject.CompareTag(TagFilter) && !hasFinishInvest)
            {
                hitSelectedTreasure = hit.collider.gameObject.GetComponent<Treasury>();
                pointerAtObject = true;
            } 
            else
            {
                hitSelectedTreasure = null;
                pointerAtObject = false;
            } 
        }
        else
        {
            hitSelectedTreasure = null;
            pointerAtObject = false;
        }
    }
    public void RemoveEnum(CursorEnum _target)
    {
        if(HasEnum(_target))
            cursorEnum &= ~_target;
        else
            Debug.LogWarning("Don't have this status : " + _target);
    }
    public void AddEnum(CursorEnum _target)
    {
        if(!HasEnum(_target))
            cursorEnum |= _target;
        else
            Debug.LogWarning("Already have this type of status : " + _target);
    }
    public bool HasEnum(CursorEnum _target)
    {
        if(_target == CursorEnum.None)
            return false;
        else
            return  (cursorEnum & _target) != 0;
    }
    public void ToggleEnum(CursorEnum _target)
    {
        cursorEnum ^= _target;
    }
    public void OnlyFlagEnum(CursorEnum _target)
    {
        cursorEnum &= 0;
        cursorEnum |= _target;
    }
    public void ClearEnum()
    {
        cursorEnum &= 0;
    }
}

[System.Serializable]
public class CursorType
{
    public string name;
    public CursorEnum representEnum;
    public Sprite sprite;
}
public enum CursorEnum
{
    None = 0,
    Interact = 1,
    Investigate = 2,
    Charging = 4
}