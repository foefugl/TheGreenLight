﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineShake : MonoBehaviour
{
    public static CinemachineShake Instance { get; private set; }
    private CinemachineVirtualCamera CinemachineVirtualCamera;
    private CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin;
    private float shakeTimer;

    private void Awake()
    {
        if(Instance == null)
            Instance = this;
        
        CinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        cinemachineBasicMultiChannelPerlin = 
            CinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();  
    }
    public void ShakeCamera(float _intensity, float _time)
    {
        // cinemachineBasicMultiChannelPerlin
        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = _intensity;
        shakeTimer = _time;
    }
    private void Update()
    {
        if(shakeTimer > 0)
            shakeTimer -= Time.deltaTime;
        
        if(shakeTimer <= 0)
        {
            shakeTimer = 0;
            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0;
        }
    }
}