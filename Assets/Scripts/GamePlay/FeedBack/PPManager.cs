﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;

public class PPManager : MonoBehaviour
{
    public static PPManager Instance;
    private PostProcessProfile  profile;
    private AndyASERangBlurPPSSettings rangeBlur;
    private void Awake()
    {
        if(Instance == null)
            Instance = this;
        profile = GetComponent<PostProcessVolume>().profile;
        profile.TryGetSettings(out rangeBlur);
    }
    public void SetRangeBlur(float _value, float _wholeLerp, float _dur)
    {
        rangeBlur.active = true;
        float lerpingValue = _value;
        float lerpingWholelerpValue = _wholeLerp;
        DOTween.To(() => lerpingValue, x => lerpingValue = x, 0, _dur)
        .OnUpdate(() =>
        {
            rangeBlur._Value.value = lerpingValue;
        });
         DOTween.To(() => lerpingWholelerpValue, x => lerpingWholelerpValue = x, 0, _dur)
        .OnUpdate(() =>
        {
            rangeBlur._WholeBulrLerp.value = lerpingWholelerpValue;
        })
        .OnComplete(() => 
        {
            rangeBlur.active = false;
        });
    }
}
