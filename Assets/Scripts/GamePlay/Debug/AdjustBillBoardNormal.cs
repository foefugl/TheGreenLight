﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustBillBoardNormal : MonoBehaviour
{
    private Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }
    private void Update() 
    {
        rend.material.SetFloat("_NormalZ", transform.forward.z);
    }
}
