﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetPointTest : MonoBehaviour
{
    public Transform location;

    public void OnDrawGizmos()
    {
        var collider = GetComponent<Collider>();

        if (!collider)
            return; // nothing to do without a collider

        Vector3 dir = (transform.position - location.position).normalized;
        float dis = Vector3.Distance(transform.position, location.position);
        Vector3 farestPoint = collider.ClosestPoint(transform.position + dir * dis);

        Gizmos.DrawSphere(location.position, 0.1f);
        Gizmos.DrawWireSphere(farestPoint, 0.1f);
    }
}
