﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class FillAmountText : MonoBehaviour
{
    public Image sliceImage;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    private void Update() 
    {
        text.text = (sliceImage.fillAmount * 100).ToString();
    }
}
