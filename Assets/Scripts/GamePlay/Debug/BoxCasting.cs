﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCasting : MonoBehaviour
{
    private void OnDrawGizmos() 
    {
        RaycastHit hit;
        bool isHit = Physics.BoxCast(transform.position, transform.lossyScale/2, transform.forward, out hit, transform.rotation, 100);
        if(isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * hit.distance);
            Gizmos.DrawWireCube(transform.position + transform.forward * hit.distance, transform.lossyScale);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, transform.forward * 100);
        }
    }
}
