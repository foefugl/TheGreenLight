﻿using System.Collections;
using UnityEngine;

public class InvestigateSystem : MonoBehaviour
{
    static public InvestigateSystem Instance {get; private set;}
    public LayerMask effectLayer;
    public bool PointerAtObject;

    [TagSelector]
    public string TagFilter = "";
    private void Awake()
    {
        if(Instance == null)
            Instance = this;    
    }

    private void Update()
    {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 50, effectLayer))
        {
            if(hit.collider.gameObject.CompareTag(TagFilter))
                PointerAtObject = true;
            else
                PointerAtObject = false;
        }
        else
        {
            PointerAtObject = false;
        }
    }
}