﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasury : MonoBehaviour
{ 
    public List<ItemPoolData> content;
    public List<ItemObject> afterStack;
    private bool treasureDataHasSend;
    private void Start()
    {
        afterStack = new List<ItemObject>();
        TakeDataFromPool();
    }
    private void TakeDataFromPool()
    {
        for(int i=0 ; i<content.Count ; i++)
        {
            content[i].Clip  = new List<ItemObject>();
            content[i].Cabinet = new GameObject().transform;
            content[i].Cabinet.parent = transform;
            content[i].Cabinet.localPosition = Vector3.zero;
            content[i].Cabinet.name = "[" + content[i].name + "]";
            for(int j=0 ; j<content[i].amount ; j++)
            {
                var currentItemObject = ItemObjectPool.Instance.Take(content[i].itemData, transform.position, transform.rotation.eulerAngles);
                content[i].Clip.Add(currentItemObject);
                currentItemObject.transform.SetParent(content[i].Cabinet);
                currentItemObject.gameObject.SetActive(false);
            }
        }
    }
    public void ResetAfterStack(List<ItemObject> target)
    {
        afterStack = new List<ItemObject>();
        for(int i=0 ; i<target.Count ; i++)
        {
            afterStack.Add(target[i]);
        }
        
    }
    public void Replenishment()
    {
        if(treasureDataHasSend)
        {
            for(int i=0 ; i<afterStack.Count ; i++)
            {
                TreasureBagSystem.Instance.Stack(afterStack[i]);
            }
        }
        else
        {
            treasureDataHasSend = true;
            for(int i=0 ; i<content.Count ; i++)
            {
                for(int j=0 ; j<content[i].amount ; j++)
                {
                    TreasureBagSystem.Instance.Stack(content[i].Clip[j]);
                }
            }
        }
        
        TreasureBagSystem.Instance.ToggleTreasureBag();
    }
}
