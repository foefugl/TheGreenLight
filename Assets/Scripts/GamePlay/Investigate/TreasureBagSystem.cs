﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TreasureBagSystem : PackSystem
{
    public static TreasureBagSystem Instance{ get; private set; }
    public bool opening;
    public GameObject TreasureBag;
    public Transform emptyItemUIStorage;
    public GameObject itemUIPrefab;
    public List<BagGrid> itemGrids;
    private List<PackItemData> itemUIs;
    private Dictionary<int, List<ItemObject>> entityContentDic;
    private Dictionary<int, PackItemData> bagGridUIDataDic;

    private void Awake()
    {
        if(Instance == null)
            Instance = this;

        entityContentDic = new Dictionary<int, List<ItemObject>>();
        bagGridUIDataDic = new Dictionary<int, PackItemData>();
        itemUIs = new List<PackItemData>();
        for(int i=0 ; i<itemGrids.Count ; i++)
        {
            PackItemData uiData = Instantiate(itemUIPrefab, emptyItemUIStorage.position, emptyItemUIStorage.rotation).GetComponent<PackItemData>();
            uiData.beLongPackSystem = this;
            uiData.transform.SetParent(emptyItemUIStorage);
            uiData.transform.localPosition = Vector3.zero;
            uiData.gameObject.SetActive(false);
            itemUIs.Add(uiData);
        }
    }
    private void Start()
    {
        if(!opening)
            TreasureBag.SetActive(false);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.V)){
            opening = !opening;
            TreasureBag.SetActive(opening);
        }
        // Debug.Log(bagGridUIDataDic.Count());
    }
    public void ToggleTreasureBag()
    {
        opening = !opening;
        TreasureBag.SetActive(opening);
    }
    public void ClearTreasureBagData()
    {
        if(entityContentDic.Count == 0)
            return;
        
        for(int i=0 ; i<entityContentDic.Count ; i++)
        {
            var packItemData = bagGridUIDataDic[entityContentDic.ElementAt(i).Key];
            packItemData.ClearData();
            packItemData.gameObject.SetActive(false);
            packItemData.transform.parent = emptyItemUIStorage;
            packItemData.transform.localPosition = Vector3.zero;
            bagGridUIDataDic.Remove(entityContentDic.ElementAt(i).Key);
        }
        for(int i=0 ; i<itemGrids.Count ; i++)
        {
            itemGrids[i].filled = false;
        }
        entityContentDic = new Dictionary<int, List<ItemObject>>();
        bagGridUIDataDic = new Dictionary<int, PackItemData>();
    }
    public void SetBackDataToObj(Treasury targetTreasury)
    {
        if(entityContentDic.Count == 0)
            return;
        List<ItemObject> tempAdd = new List<ItemObject>();
        for(int i=0 ; i<entityContentDic.Count ; i++)
        {
            tempAdd.AddRange(entityContentDic.ElementAt(i).Value);
        }
        targetTreasury.ResetAfterStack(tempAdd);
    }
    public void Stack(ItemObject good)
    {
        good.JustBeCollected(transform);
        AddToBag(good);
    }
    public override void AddToBag(ItemObject itemObject)
    {
        int _id = itemObject.itemData.ID;
        if(IsEntityHaveIt(_id))
        {
            entityContentDic[_id].Add(itemObject);
        }
        else
        {
            List<ItemObject> newList = new List<ItemObject>();
            newList.Add(itemObject);
            entityContentDic.Add(_id, newList);
        }
        AddBagInfo(itemObject);
    }
    private void AddBagInfo(ItemObject _itemObj)
    {
        if(bagGridUIDataDic.ContainsKey(_itemObj.itemData.ID))
        {
            bagGridUIDataDic[_itemObj.itemData.ID].UpdateGridContent(_itemObj, entityContentDic[_itemObj.itemData.ID].Count);
        }
        else
        {
            for(int i=0 ; i<itemGrids.Count ; i++)
            {
                if(!itemGrids[i].filled)
                {
                    itemGrids[i].filled = true;
                    var emptyUIBeSelected = SelectEmptyItemUI();
                    emptyUIBeSelected.beLongPackSystem = this;
                    emptyUIBeSelected.gameObject.SetActive(true);
                    emptyUIBeSelected.itemImage.transform.parent = itemGrids[i].transform;
                    emptyUIBeSelected.itemImage.rectTransform.anchoredPosition = Vector3.zero;
                    // Debug.Log("xxx : " + entityContentDic[_itemObj.itemData.ID].Count);
                    emptyUIBeSelected.UpdateGridContent(_itemObj, entityContentDic[_itemObj.itemData.ID].Count);
                    bagGridUIDataDic.Add(_itemObj.itemData.ID, emptyUIBeSelected);
                    break;
                }
            }
        }
    }
    public override void RemoveFromBag(ItemObject _itemOb, bool throwOut)
    {
        if(IsEntityHaveIt(_itemOb.itemData.ID))
        {
            List<ItemObject> list = entityContentDic[_itemOb.itemData.ID];
            var topObj = list.Last();

            if(throwOut)
                topObj.BeThrowed();

            if(list.Count == 1)
                entityContentDic.Remove(_itemOb.itemData.ID);

            list.RemoveAt(list.Count - 1);
        }
        RemoveBagInfo(_itemOb);
    }
    private void RemoveBagInfo(ItemObject itemObject)
    {
        PackItemData packItemData = bagGridUIDataDic[itemObject.itemData.ID];
        if(entityContentDic.ContainsKey(itemObject.itemData.ID))
        {
            packItemData.UpdateGridContent(itemObject, entityContentDic[itemObject.itemData.ID].Count);
        }
        else
        {
            packItemData.ClearData();
            packItemData.gameObject.SetActive(false);
            packItemData.transform.parent = emptyItemUIStorage;
            packItemData.transform.localPosition = Vector3.zero;
            bagGridUIDataDic.Remove(itemObject.itemData.ID);
        }
    }
    private PackItemData SelectEmptyItemUI()
    {
        PackItemData answer = null;
        for(int i=0 ; i<itemUIs.Count ; i++)
        {
            if(itemUIs[i].gameObject.activeSelf == false)
            {
                answer = itemUIs[i];
                break;
            }
        }
        return answer;
    }
    private bool IsEntityHaveIt(int id)
    {
        return entityContentDic.ContainsKey(id);
    }
}
