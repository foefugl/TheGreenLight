﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingOrderToMesh : MonoBehaviour
{
    public int orderIndex = 1;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().sortingOrder = orderIndex;
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
