﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;
using DG.Tweening;

public class SimpleAI : MonoBehaviour
{
    public Transform nextPoint;
    public enum BehaviourState
    {
        Chasing,
        ChaseFall,
        Wandering,
        Attack, 
        Dead
    }
    public BehaviourState state = BehaviourState.Wandering;
    public ParticleSystem listenVFX;
    public MonsterVisibleState VisualState;
    public LayerMask floorLayer;
    public float ViewAngle = 45;
    public float ViewDisLimit = 6.5f;
    public float ListenRadius = 4;
    public float WanderDis = 4;
    public NavMeshAgent agent;
    private Renderer[] allSkinRenderers;
    private ParticleSystemRenderer psRend;
    [SerializeField] private Animator animator;
    private Vector3 resiPoint;
    private Transform playerTrans;
    private float normalSpd;
    private void Awake()
    {
        allSkinRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        psRend = listenVFX.GetComponent<ParticleSystemRenderer>();
        playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
        SetMonsterVisibleSate(MonsterVisibleState.UnVisible);
    }
    private void Start()
    {
        resiPoint = transform.position;
        normalSpd = agent.speed;
        StartCoroutine(WanderingCoroutine());
    }
    private void Update()
    {
        listenVFX.transform.position = -Camera.main.transform.forward * 5 + transform.position;
        
        
        if(agent.velocity.magnitude <= 0.165f){
            animator.SetFloat("hSpeed", 0);
            animator.SetFloat("lastDir", (transform.forward.x > 0)? 1 : -1 );
        }
        else{
            animator.SetFloat("hSpeed", agent.velocity.x);
            // lastVelocity = agent.velocity;
        }

        float disToPlayer = Vector3.Distance(playerTrans.position, transform.position);
        Vector3 dirToTarPlayer = (playerTrans.position - transform.position).normalized;
        dirToTarPlayer = new Vector3(dirToTarPlayer.x, 0, dirToTarPlayer.z);
        RaycastHit hitDetectPlayer;
        Physics.Raycast(transform.position, dirToTarPlayer, out hitDetectPlayer, disToPlayer);

        if(state == BehaviourState.Chasing && hitDetectPlayer.collider.gameObject != playerTrans.gameObject)
        {
            Debug.Log("View Block ! ");
            StopAllCoroutines();
            StartCoroutine(ChaseFall());
        }
        
        if(disToPlayer <= ViewDisLimit)
        {
            // Can hear Player
            float angleToAngle = Vector3.Angle(transform.forward, dirToTarPlayer);
            if(angleToAngle > (ViewAngle/2))
            {
                Debug.Log("Can listen player !");
                // Vector3 dirToPlayer = (playerTrans.position - transform.position).normalized;
                // Debug.DrawRay(transform.position, dirToPlayer, Color.yellow);
                // agent.enabled = false;
                // transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(dirToPlayer), Time.deltaTime);
                // StartCoroutine(HearPlayer());
            }
            else
            {
                if(hitDetectPlayer.collider.gameObject == playerTrans.gameObject)
                {
                    // Can See Player
                    Debug.Log("Can See Player");
                    StopAllCoroutines();
                    AgentStopAtPoint();
                    StartCoroutine(ChasingPlayer());
                }
            }
        }
    }
    public void GetHittedTurn(Vector3 atkDir)
    {
        Debug.Log("Turn");
        StopAllCoroutines();
        AgentStopAtPoint();
        Vector3 zombieForward = transform.parent.forward;
        DOTween.To(() => zombieForward, x => zombieForward = x, -atkDir, 0.5f)
            .OnUpdate(() =>
            {
                transform.parent.forward = zombieForward;
            });
    }
    private void AgentStopAtPoint() 
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 5, floorLayer))
        {
            agent.SetDestination(hit.point);
        }
    }
    private IEnumerator HearPlayer()
    {
        while(state != BehaviourState.Chasing)
        {
            
            yield return null;
        }
    }
    private IEnumerator ChaseFall()
    {
        state = BehaviourState.ChaseFall;

        while(true)
        {
            if(agent.velocity.magnitude == 0)
            {
                StopAllCoroutines();
                StartCoroutine(WanderingCoroutine());
                break;
            }
            yield return null;
        }
    }
    private IEnumerator ChasingPlayer()
    {
        Debug.Log("Chasing Player");
        state = BehaviourState.Chasing;
        agent.speed = normalSpd * 1.5f;

        while(true)
        {
            RaycastHit hit;
            if(Physics.Raycast(playerTrans.position + Vector3.up, Vector3.down, out hit, 5, floorLayer)){
                agent.SetDestination(hit.point);
            }
            yield return null;
        }
    }
    // private IEnumerator AttackPlayer()
    // {
    //     Debug.Log("Attack !");
    //     state = BehaviourState.Attack;

    // }
    private IEnumerator WanderingCoroutine()
    {
        Debug.Log("Wandering");
        state = BehaviourState.Wandering;
        agent.speed = normalSpd;
        resiPoint = transform.position;
        while(true)
        {
            float randomTime = Random.Range(3.5f, 7.0f);
            Vector3 randomValue = Random.insideUnitCircle * WanderDis;
            Vector3 targetPoint = new Vector3(randomValue.x, 0, randomValue.y) + resiPoint;
            RaycastHit hit;
            if(Physics.Raycast(targetPoint + Vector3.up, Vector3.down, out hit, 5, floorLayer)){
                agent.SetDestination(hit.point);
                nextPoint.position = hit.point;
            }

            yield return new WaitForSeconds(randomTime);
        }
    }
    public void SetMonsterVisibleSate(MonsterVisibleState targetState)
    {
        if(targetState == MonsterVisibleState.UnVisible)
            SetListenVFX(false);
        else if(targetState == MonsterVisibleState.OnlyCanListen)
            SetListenVFX(true);
        else if(targetState == MonsterVisibleState.Visible)
            SetListenVFX(false);

        VisualState = targetState;
    }
    private void SetAllRenderer(bool targetState)
    {
        for(int i=0 ; i<allSkinRenderers.Length ; i++)
        {
            allSkinRenderers[i].enabled = targetState;
        }
    }
    private void SetListenVFX(bool targetState)
    {
        if(targetState && !psRend.enabled)
            psRend.enabled = true;
        else if(!targetState && psRend.enabled)
            psRend.enabled = false;
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.green;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, transform.forward, 360, ListenRadius);

        Handles.color = Color.cyan;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(transform.position, transform.up, Quaternion.Euler(Vector3.up * -ViewAngle/2) * transform.forward, ViewAngle, ViewDisLimit);

        Handles.color = Color.red;
        Handles.color = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.0125f);
        Handles.DrawSolidArc(resiPoint, transform.up, transform.forward, 360, WanderDis);
    }
#endif
}
