﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class InverseDrawMesh : MonoBehaviour
{
    public Transform playerTrans;
    public LayerMask obstacleLayer;
    public MeshFilter meshFilter;
    public float viewAngle, viewRadius, upperOffset = 0.1f, selfRadius = 10;
    public Vector3 offset;

    [SerializeField]
    private float meshResolution;
    private float heightToFloor;
    private Mesh viewMesh;
    private List<Vector3> viewPoints;
    private List<GameObject> wallSideObjects;
    private Material generalMat;
    private void Awake()
    {
        viewMesh = new Mesh();
        generalMat = GetComponent<MeshRenderer>().material;
        viewMesh.name = "View Mesh";
        meshFilter.mesh = viewMesh;
    }

    private void LateUpdate()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position + Vector3.up * upperOffset, Vector3.down, out hit, 5, obstacleLayer)){
            heightToFloor = hit.distance;
        }
        transform.position = playerTrans.position;
        transform.rotation = Quaternion.Euler(0, playerTrans.rotation.eulerAngles.y, 0);
        Vector3 _dir = DirFromAngle(transform.rotation.eulerAngles.y, true);
        DrawOfView();
    }
    private void DrawOfView() 
    {
        int stepCount = Mathf.RoundToInt(360.0f * meshResolution);
        float stepAngleSize = 360.0f  / stepCount;
        viewPoints = new List<Vector3>();
        bool rayHitCollider = false;
        Vector3 lastHitPoint = Vector3.zero;
        float lastDisFromFog = 0;
        wallSideObjects = new List<GameObject>();


        for (int i=0; i<=stepCount; i++) 
        {
            float angle = transform.eulerAngles.y - 360.0f  / 2 + stepAngleSize * i;
            Vector3 dir = DirFromAngle(angle, false);
            Vector3 rayStartPointAtEdge = transform.position + dir * viewRadius + Vector3.up * upperOffset;

                
            float filterAngle = (Mathf.Abs(angle) >= 180) ? angle - 360 : angle;
            if(Mathf.Abs(filterAngle) <= viewAngle/2)
            {
                RaycastHit[] hits = Physics.RaycastAll(rayStartPointAtEdge, -dir, viewRadius, obstacleLayer);
                if (hits.Length != 0)
                {
                    ViewCastInfo edgePoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                    RaycastHit closetPoint = FindTheClosetHit(hits, transform.position + Vector3.up * upperOffset);

                    if (Mathf.Abs(lastDisFromFog - closetPoint.distance) > 1 && rayHitCollider)
                    {
                        CreateSideWall(lastHitPoint, closetPoint.point, dir);
                    }

                    lastHitPoint = closetPoint.point;
                    lastDisFromFog = closetPoint.distance;
                    viewPoints.Add(closetPoint.point);
                    viewPoints.Add(edgePoint.point);
                    
                    if(!rayHitCollider)
                    {
                        rayHitCollider = true;
                        CreateSideWall(edgePoint.point, lastHitPoint, dir);
                    }
                }
                else
                {
                    ViewCastInfo edgePoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                    ViewCastInfo innerPoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                    viewPoints.Add(edgePoint.point);
                    viewPoints.Add(innerPoint.point);
                    if(rayHitCollider){
                        rayHitCollider = false;
                        CreateSideWall(edgePoint.point, lastHitPoint, dir);
                    }
                }
            }
            else
            {
                ViewCastInfo edgePoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                ViewCastInfo innerPoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                viewPoints.Add(edgePoint.point);
                viewPoints.Add(innerPoint.point);
            }
        }
        StartCoroutine(ClearSideWallRender());

        int vertexCount = viewPoints.Count;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        for (int i = 0; i < vertexCount; i++)
        {
            vertices[i] = transform.InverseTransformPoint(viewPoints[i]);
            if(i%2 == 0 && i<vertexCount-2)
            {
                triangles[i * 3] = i;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 3;
                triangles[i * 3 + 3] = i;
                triangles[i * 3 + 4] = i + 3;
                triangles[i * 3 + 5] = i + 2;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;

        viewMesh.RecalculateNormals();
    }
    private RaycastHit FindTheClosetHit(RaycastHit[] _hits, Vector3 _refOrigin)
    {
        Dictionary<RaycastHit, float> disPairDic = new Dictionary<RaycastHit, float>();
        for(int i=0 ; i<_hits.Length ; i++)
        {
            disPairDic.Add(_hits[i], _hits[i].distance);
        }
        var reorder = disPairDic.OrderByDescending(piar => piar.Value);
        return reorder.FirstOrDefault().Key;
    }
    private RaycastHit[] FindTopTwoCloseHit(RaycastHit[] _hits, Vector3 _refOrigin) 
    {
        Dictionary<RaycastHit, float> disPairDic = new Dictionary<RaycastHit, float>();
        for (int i = 0; i < _hits.Length; i++)
        {
            disPairDic.Add(_hits[i], _hits[i].distance);
        }
        var reorder = disPairDic.OrderByDescending(piar => piar.Value);
        return new RaycastHit[] { reorder.FirstOrDefault().Key, reorder.ToList()[1].Key };
    }
    private void CreateSideWall(Vector3 _edgePoint, Vector3 _lastHitPoint, Vector3 _dir)
    {
        GameObject sideWall = new GameObject("SideWall");
        wallSideObjects.Add(sideWall);
        Mesh wallMesh = new Mesh();
        var filter = sideWall.AddComponent<MeshFilter>();
        var rend = sideWall.AddComponent<MeshRenderer>();
        filter.mesh = wallMesh;
        rend.material = generalMat;
        Vector3[] wallMeshPoints = new Vector3[4];
        int[] wallTriangles = new int[(wallMeshPoints.Length-2) * 3];
        wallMeshPoints[0] = _edgePoint;
        wallMeshPoints[1] = _edgePoint - Vector3.up * heightToFloor;
        wallMeshPoints[2] = _lastHitPoint - Vector3.up * heightToFloor;
        wallMeshPoints[3] = _lastHitPoint;
        if(_dir.x > 0)
        {
            wallTriangles[0] = 0;
            wallTriangles[1] = 1;
            wallTriangles[2] = 2;
            wallTriangles[3] = 0;
            wallTriangles[4] = 2;
            wallTriangles[5] = 3;
        }
        else
        {
            wallTriangles[0] = 0;
            wallTriangles[1] = 2;
            wallTriangles[2] = 1;
            wallTriangles[3] = 0;
            wallTriangles[4] = 3;
            wallTriangles[5] = 2;
        }

        wallMesh.Clear();
        wallMesh.vertices = wallMeshPoints;
        wallMesh.triangles = wallTriangles;

        wallMesh.RecalculateNormals();
    }
    private IEnumerator ClearSideWallRender()
    {
        yield return null;
        for(int i=0 ; i<wallSideObjects.Count ; i++)
        {
            Destroy(wallSideObjects[i]);
        }
    } 
    private Vector3 DirFromAngle(float angleInDegress, bool angleIsGlobal) 
    {
        if (!angleIsGlobal) {
            angleInDegress += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegress * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegress * Mathf.Deg2Rad));
    }
    private struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }
#if UNITY_EDITOR
    // private void OnDrawGizmos()
    // {
    //     if (EditorApplication.isPlaying) 
    //     {
    //         if( viewPoints.Count != 0)
    //         {
    //             for (int i=0; i< viewPoints.Count; i++) 
    //             {
    //                 Handles.Label(viewPoints[i], i.ToString());
    //             }
    //         }
            
    //         if(debugList.Count != 0)
    //         {
    //             for (int i=0; i< debugList.Count; i++) 
    //             {
    //                 Handles.Label(debugList.ElementAt(i).Value, debugList.ElementAt(i).Key.ToString());
    //             }
    //         }
    //     }
    // }
#endif
}
