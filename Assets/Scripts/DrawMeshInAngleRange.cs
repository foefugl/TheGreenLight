﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawMeshInAngleRange : MonoBehaviour
{
    public Transform playerTrans;
    public LayerMask obstacleLayer;
    public MeshFilter meshFilter;
    public Vector3 offset;
    public float viewAngle, viewRadius, upperOffset = 0.1f, selfRadius = 2;
    [SerializeField]
    private float meshResolution;
    private List<Vector3> coverPoints;
    private Mesh coverMesh;

    private void Awake()
    {
        coverMesh = new Mesh();
        coverMesh.name = "coverMesh";
        meshFilter.mesh = coverMesh;
    }
    private void LateUpdate()
    {
        transform.position = playerTrans.position;
        transform.rotation = Quaternion.Euler(0, playerTrans.rotation.eulerAngles.y, 0);
        Vector3 _dir = DirFromAngle(transform.rotation.eulerAngles.y, true);
        DrawArrow.ForDebug(transform.position, _dir);
        DrawOfView();
    }
    private void DrawOfView() 
    {
        int stepCount = Mathf.RoundToInt(360.0f * meshResolution);
        float stepAngleSize = 360.0f  / stepCount;
        coverPoints = new List<Vector3>();
        Vector3 lastHitPoint = Vector3.zero;

        for (int i=0; i<=stepCount; i++) 
        {
            float angle = transform.eulerAngles.y - 360.0f  / 2 + stepAngleSize * i;
            Vector3 dir = DirFromAngle(angle, false);
            Vector3 rayStartPointAtEdge = transform.position + dir * viewRadius + Vector3.up * upperOffset;
                
            float filterAngle = (Mathf.Abs(angle) >= 180) ? angle - 360 : angle;
            if(Mathf.Abs(filterAngle) >= viewAngle/2)
            {
                ViewCastInfo edgePoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                ViewCastInfo innerPoint = new ViewCastInfo(true, rayStartPointAtEdge + offset - dir * (viewRadius - selfRadius), selfRadius , angle);
                coverPoints.Add(edgePoint.point);
                coverPoints.Add(innerPoint.point);
            }
            else
            {
                ViewCastInfo edgePoint = new ViewCastInfo(true, rayStartPointAtEdge, viewRadius, angle);
                ViewCastInfo innerPoint = new ViewCastInfo(true, rayStartPointAtEdge, selfRadius , angle);
                coverPoints.Add(edgePoint.point);
                coverPoints.Add(innerPoint.point);
            }
        }

        int vertexCount = coverPoints.Count;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        for (int i = 0; i < vertexCount; i++)
        {
            vertices[i] = transform.InverseTransformPoint(coverPoints[i]);
            if(i%2 == 0 && i<vertexCount-2)
            {
                triangles[i * 3] = i;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 3;
                triangles[i * 3 + 3] = i;
                triangles[i * 3 + 4] = i + 3;
                triangles[i * 3 + 5] = i + 2;
            }
        }

        coverMesh.Clear();
        coverMesh.vertices = vertices;
        coverMesh.triangles = triangles;

        coverMesh.RecalculateNormals();
    }
    private Vector3 DirFromAngle(float angleInDegress, bool angleIsGlobal) 
    {
        if (!angleIsGlobal) {
            angleInDegress += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(angleInDegress * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegress * Mathf.Deg2Rad));
    }
    private struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }
}
