﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDamage : MonoBehaviour
{
    public float pushForce = 10;
    public GameObject HittedVFX;
    private Rigidbody monsterRb;
    private SimpleAI ai;

    private void Start()
    {
        monsterRb = GetComponentInParent<Rigidbody>();
        ai = GetComponent<SimpleAI>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.collider.gameObject.layer != LayerMask.NameToLayer("Bullet"))
            return;
        
        Vector3 attackDir = (transform.position - CharacterController.Instance.transform.position).normalized;
        ai.GetHittedTurn(attackDir);
        
        GameObject hittedVfx = Instantiate(HittedVFX, other.contacts[0].point, Quaternion.identity);
        hittedVfx.transform.rotation = Quaternion.Euler(-other.collider.GetComponent<Rigidbody>().velocity);
        var vfxs = hittedVfx.GetComponentsInChildren<ParticleSystem>();
        foreach (var vfx in vfxs){
            vfx.Play();
        }
        Destroy(hittedVfx, 3);
    }
}
