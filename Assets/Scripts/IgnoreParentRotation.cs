﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreParentRotation : MonoBehaviour
{
    private Quaternion presetRot;

    // Start is called before the first frame update
    void Awake()
    {
        presetRot = transform.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.rotation = presetRot;
    }
}
