﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Monster2ProceduralAnimation : MonoBehaviour
{
    public bool rotateActive, scaleActive;
    [Range(0.0f, 1)]
    public float breatheValue = 0.2f;
    public float InhaleT = 0.65f, ExhaleT = 1;
    [Range(0, 3)]
    public float interval = 1;
    [Range(0, 0.1f)]
    public float shakePow = 0.1f;
    [Range(0, 0.5f)]
    public float shakeDuration = 0.5f;
    private Vector3 oriScale;
    private Tween rotTwn, moveTwn, scaleTwn;

    // Start is called before the first frame update
    void Start()
    {
        oriScale = transform.localScale;
        if(scaleActive)
            StartCoroutine(ScaleTwnCoroutine());
        if(rotateActive)
            StartCoroutine(RotateTwnCoroutin());
    }
    IEnumerator RotateTwnCoroutin()
    {
        float randDelay = Random.Range(0.0f, 3.0f);
        yield return new WaitForSeconds(randDelay);
        while(true)
        {
            rotTwn = transform.DOShakeRotation(shakeDuration * 2f, 180, 10)
            .OnUpdate(() => { transform.rotation = Quaternion.Euler(0, transform.rotation.z * Mathf.Sin(45) * 2, transform.rotation.z * -Mathf.Cos(45) * 2); });
            yield return new WaitForSeconds(interval);
        }
    }
    IEnumerator ScaleTwnCoroutine()
    {
        float randDelay = Random.Range(0.0f, 3.0f);
        bool endFlag = false;
        yield return new WaitForSeconds(randDelay);
        while(true)
        {
            scaleTwn = transform.DOScale(oriScale * (1 + breatheValue), InhaleT)
            .OnComplete(() => 
            {
                scaleTwn = transform.DOScale(oriScale, ExhaleT)
                .OnComplete(() => 
                {
                    endFlag = true;
                });
            })
            .OnStart(() => { endFlag = false; });
            yield return new WaitUntil(() => endFlag);
        }
    }
}
